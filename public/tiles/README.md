# MAdB, los datos
[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License_for_Data-CC%20BY--NC--SA%204.0-008080.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

[Abel Vázquez Montoro](https://www.linkedin.com/in/abelvazquez/) 2020

[![img](../assets/img/madb.svg)](https://madb.netlify.com/)

## Qué es esto?

Esta carpeta contiene los datos usados en [![MAdB](https://img.shields.io/badge/MA-dB-ff1d8e?labelColor=008080)](https://madb.netlify.com/), una herramienta para dar visibilidad a los efectos sobre la salud del ruido del tráfico en la ciudad de Madrid.

## Licencias

Los **datos** de [![MAdB](https://img.shields.io/badge/MA-dB-ff1d8e?labelColor=008080)](https://madb.netlify.com/) están bajo licencia [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0).