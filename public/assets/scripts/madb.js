/*jshint esversion: 6 */
(function () {
    'use strict';

    window.madb = window.madb || {};

    const
        _showinfo = (t,s) => {
            let cb = document.createElement('div');
            cb.className = 'cbutton as-hot';
            cb.innerHTML='<i class="icofont-close"></i>';    
            cb.addEventListener('click', e=>{if (!!madb.dialog.open) madb.dialog.close();});        
            if (!!madb.dialog.open) madb.dialog.close();
            madb.dialog.innerHTML = t;
            madb.dialog.appendChild(cb);
            if (!!!madb.dialog.open) madb.dialog.showModal();
            _addinfo('dialog .ampliar', '.info-method');
            if(s !== void 0) breath(`dialog ${s}`);
        },
        _addinfo = (se, st,s) =>{
            const 
                o = document.querySelector(se),
                t = document.querySelector(st).outerHTML;
            if (!!!o) return false;
            if (o.classList.contains('residesc') || o.classList.contains('noisedesc') || o.classList.contains('as-vis') || o.classList.contains('sharer')){
                o.style.cursor = 'help';
            }else{
                o.classList.add('as-help');
            } 
            o.addEventListener('click', e =>{
                _showinfo(t,s);
                e.stopPropagation();
                e.preventDefault();
            }, false); 
        },        
        _lookup = e =>{
            const 
                s = document.querySelector('#lookup'),
                t = s.value;
            if (t.length == 0) return false;
            if (t.length < 7){
                _showinfo('<h3 class="as-title">El término de búsqueda es demasiado corto, no existen direcciones de menos de 7 caracteres.</h3>')
                return false;
            }
            s.classList.add('searching');
            madb.minions.lookup.postMessage(t);
        },
        pitcher = e => {
            const
                z = e.getZoom(),
                p = e.getPitch(),
                midp = 9 * (z - 12);
            let summary = document.querySelector('.fsummary');
            if (madb.z == z) return;
            if (z < 13 && p != 0) {
                e.setPitch(0);
            } else if (z >= 13 && z < 17 && p != midp) {
                e.setPitch(midp);
            } else if (z >= 17 && p != 45) {
                e.setPitch(45);
            }
            if(summary.innerText == 'Acércate en el mapa y haz click en un edificio para ver su información detallada' && z>=15){
                summary.innerHTML='<h1 class="as-title">Haz click en un edificio para ver su información detallada</h1>';
            }
            if(summary.innerText == 'Haz click en un edificio para ver su información detallada' && z < 15){
                summary.innerHTML='<h1 class="as-title"><span class="as-info as-font--medium">Acércate en el mapa</span> y haz click en un edificio para ver su información detallada</h1>';
            }
            madb.z = z;
            madb.p = p;
        },
        newdata = e => {
            const layer = (madb.z >= 15)? '3d-buildings': 'buildings' ;
            madb.data.raw = madb.day.queryRenderedFeatures({ layers: [layer] }).map(b => b.properties);
            madb.minions.stats.postMessage([{
                data: madb.data.raw,
                fields: madb.data.fields
            }]);
        },
        getfloor = e =>{
            let
                inc = -3,
                vent = -15.0,
                l = ['ld_mx', 'le_mx', 'ln_mx', 'lden_mx', 'ld_mn', 'le_mn', 'ln_mn', 'lden_mn', 'sleep_mx', 'live_mx', 'sleep_mn', 'live_mn'],
                 // NORMATIVA DE RUIDO Y VIBRACIONES, CAM
                inmision = {
                    'agriculture': [999,999],
                    'industrial': [60,55],
                    'office': [45,45],
                    'publicServices': [40,30],
                    'residential': [35,30],
                    'retail': [50,50]
                },
                use = madb.data.building.use,
                nsummary = document.querySelector('.nsummary'),
                rheader = document.querySelector('.rheader'),
                fs = document.querySelector('.fselector'),
                fn = (fs !== null)? fs.value : -1,
                f = {'f':fn},
                v = {},
                disturbance = (den, n) =>{
                    let r = {};
                    r.ha = 78.9270 - 3.1162 * den + 0.0342 * den * den;
                    if(n<40){
                        r.sd = 0;
                        r.level = '';
                    }else if(n<45){ //LSD
                        r.sd =  -8.4 + 0.16 * n + 0.01081 * n * n;
                        r.level = 'leves ';
                    }else if(n<65){ //LD
                        r.sd = 13.8 - 0.85 * n + 0.01670 * n * n;
                        r.level = 'moderadas ';
                    }else{ //HSD
                        r.sd = 20.8 - 1.05 * n + 0.01486 * n * n;
                        r.level = 'severas ';
                    }
                    return r;
                },
                dist,
                nmsg;
            for (const key in madb.data.building){
                if (l.indexOf(key) > - 1){
                    f[key] = Math.round(10*(madb.data.building[key] + inc - fn * 0.36))/10; 
                    v[key] = Math.round(10*(madb.data.building[key] + inc + vent - fn * 0.36))/10; 
                }
            }
            dist = disturbance(f.lden_mx, f.ln_mx);
            nmsg = (dist.sd==0)?'': ` y <span${(dist.level == 'severas ' && dist.sd>=3)?' class="as-hot as-font--medium"':''}>el <span class="as-font--medium">${dist.sd.toFixed(1)}%</span> tiene alteraciones <span class="as-font--medium">${dist.level}</span>del sueño a causa del ruido</span>`;
            rheader.innerHTML = `
            <p class="as-body">Diferencia de la <a class="as-help" href='https://es.wikipedia.org/wiki/Riesgo_atribuible#Proporción_de_riesgo_atribuible' target=blank>proporción de riesgo atribuible (RA%)</a> para la salud derivados de residir en 
            <span class="as-cold as-font--medium">${(fn>-1)? (fn==0)? 'el Bajo de':`la ${fn}ª planta de`: ''}</span> este edificio en relación a a un edificio expuesto al ruido medio, tanto para la zona visible en el mapa como para todo Madrid <i class="icofont-info-circle decibels2"></i>:</p>
            <div class="container-loader">
                <div class="loader"></div>
            </div>
            <table class="as-table as-table--stripped valores rhe">
            <colgroup>
                <col>
                <col>
                <col>
                <col>
                <col>
            </colgroup> 
            <thead>
                <tr>
                <th>Fachada...</th>
                <th colspan="2">Más expuesta</th>
                <th colspan="2">Menos expuesta</th>
                </tr>
                <tr>
                <th>En relación a...</th>
                <th>Zona</th>
                <th>Madrid</th>
                <th>Zona</th>
                <th>Madrid</th>
                </tr>
            </thead>
            </table>
            `;
            nsummary.innerHTML = `
            <p class="as-body">
                Valores de ruido incidente, en <a class="as-help" href='https://es.wikipedia.org/wiki/Contaminaci%C3%B3n_ac%C3%BAstica#Nivel_de_presi%C3%B3n_sonora_continuo_equivalente_(Leq)' target=blank> decibelios (L<sub>eq</sub>)</a> <i class="icofont-info-circle decibels"></i>:
                </p>
                <table class="as-table as-table--stripped valores">
                <colgroup>
                    <col>
                    <col>
                    <col>
                    <col>
                    <col>
                    <col>
                    <col>
                </colgroup> 
                <thead>
                    <tr>
                    <th></th>
                    <th colspan="2"><i class="icofont-external-link"></i> Exterior</th>
                    <th colspan="2"><i class="icofont-external"></i> Interior</th>
                    <th colspan="2"><i class="icofont-external"></i> Int. ventanas abiertas</th>
                    </tr>
                    <tr>
                    <th>Fachada</th>
                    <th><i class='icofont-sun-alt'></i><i class='icofont-moon'></i></th>
                    <th><i class='icofont-moon'></i></th>
                    <th><i class='icofont-sun-alt'></i></th>
                    <th><i class='icofont-moon'></i></th>
                    <th><i class='icofont-sun-alt'></i></th>
                    <th><i class='icofont-moon'></i></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td>Más expuesta</td>
                    <td${(f.lden_mx>=53)?' class="notgood"':''}>${f.lden_mx}</td>
                    <td${(f.ln_mx>=45)?' class="notgood"':''}>${f.ln_mx}</td>
                    <td${(f.live_mx>=inmision[use][0])?' class="notgood"':''}>${f.live_mx}</td>
                    <td${(f.sleep_mx>=inmision[use][1])?' class="notgood"':''}>${f.sleep_mx}</td>
                    <td${(f.ld_mx + vent>=inmision[use][0])?' class="notgood"':''}>${v.ld_mx}</td>
                    <td${(f.ln_mx + vent>=inmision[use][1])?' class="notgood"':''}>${v.ln_mx}</td>
                    </tr>
                    <tr>
                    <td>Menos expuesta</td>
                    <td${(f.lden_mn>=53)?' class="notgood"':''}>${f.lden_mn}</td>
                    <td${(f.ln_mn>=45)?' class="notgood"':''}>${f.ln_mn}</td>
                    <td${(f.live_mn>=inmision[use][0])?' class="notgood"':''}>${f.live_mn}</td>
                    <td${(f.sleep_mn>=inmision[use][1])?' class="notgood"':''}>${f.sleep_mn}</td>
                    <td${(f.ld_mn + vent>=inmision[use][0])?' class="notgood"':''}>${v.ld_mn}</td>
                    <td${(f.ln_mn + vent>=inmision[use][1])?' class="notgood"':''}>${v.ln_mn}</td>
                    </tr>
                </tbody>
                </table>
                <p class="as-body">
                    En este edificio, <span${(dist.ha >=10)?' class="as-hot as-font--medium"':''}>un <span class="as-font--medium">${dist.ha.toFixed(1)}%</span> de sus residentes está muy molesto por el ruido</span>${nmsg} <i class="icofont-info-circle molesta"></i>.
                </p>
            `;
            _addinfo('.decibels', '.info-db');
            _addinfo('.decibels2', '.info-db2');
            _addinfo('.molesta', '.info-molesta');
            madb.data.floor = f;
        },
        getrisks = e =>{
            if (madb.data.building.ld_mx == void 0) return false;
            getfloor();
            const
                b = madb.data.floor,
                s = madb.data.summary.map(k=>k.median),
                // promedios de los globales para ambas fachadas, precalculado
                madrid = {ld: 56, ln: 51, lden: 59, live: 30, sleep: 25},
                // promedios de los locales para ambas fachadas
                zona = {
                    ld: 0.5*(s[3]+s[7]), 
                    ln: 0.5*(s[5]+s[9]), 
                    lden: 0.5*(s[6]+s[10]),
                    live: 0.5*(s[14]+s[16]),
                    sleep: 0.5*(s[13]+s[15])
                },
                f = (r, k, t) => {
                    const
                        p = `${r.param}_m${t}`,
                        f = r.func;
                    let n;
                    if (f === void 0){
                        let m = (r.threshold !== void 0 && b[p] >= r.threshold)? r.mult2 : r.mult;
                        n = r.factor * m * (b[p] - k);
                    }else{
                        n = (f(b[p]) - f(k)) / f(k);
                    }                        
                    return Math.round(n*10) /10;
                };
                
            let 
            rcontent ='<table class="as-table as-table--stripped risktable"><colgroup><col><col><col><col><col></colgroup><thead><tr><th colspan="5"><div><span class="rhi"><i class="icofont-hospital icofont-2x"></i></span><span class="rh">Ingresos de urgencias</span></th></tr></thead><tbody>$emergency</tbody><thead><tr><th colspan="5"><div><span class="rhi"><i class="icofont-hospital icofont-2x"></i><i class="icofont-kids-scooter icofont-2x"></i></span><span class="rh">Ingresos de urgencias infantiles (0-9 años)</span></th></tr></thead><tbody>$emergency_kids</tbody><thead><tr><th colspan="5"><div><span class="rhi"><i class="icofont-ambulance-cross icofont-2x"></i></span><span class="rh">Llamadas al 112</span></th></tr></thead><tbody>$calls_112</tbody><thead><tr><th colspan="5"><div><span class="rhi"><i class="icofont-heart-beat icofont-2x"></i></span><span class="rh">Mortalidad por causas cardiovasculares</span></th></tr></thead><tbody>$mortality_heart</tbody><thead><tr><th colspan="5"><div><span class="rhi"><i class="icofont-heart-beat icofont-2x"></i></span><span class="rh">Mortalidad por causas cardiovasculares en mayores de 65 años</span></th></tr></thead><tbody>$mortality_heart_65plus</tbody><thead><tr><th colspan="5"><div><span class="rhi"><i class="icofont-lungs icofont-2x"></i></span><span class="rh">Mortalidad por causas respiratorias en mayores de 65 años</span></th></tr></thead><tbody>$mortality_respiratory_65plus</tbody><thead><tr><th colspan="5"><div><span class="rhi"><i class="icofont-blood-drop icofont-2x"></i></span><span class="rh">Mortalidad por diabetes en mayores de 65 años</span></th></tr></thead><tbody>$mortality_diabetes_65plus</tbody><thead><tr><th colspan="5"><div><span class="rhi"><i class="icofont-baby icofont-2x"></i></span><span class="rh">Complicaciones en el parto</span></th></tr></thead><tbody>$adverse_births</tbody><thead><tr><th colspan="5"><div><span class="rhi"><i class="icofont-hospital icofont-2x"></i><i class="icofont-brain-alt icofont-2x"></i></span><span class="rh">Admisiones hospitalarias por enfermedades neurodegenerativas</span></th></tr></thead><tbody>$hospital_admissions</tbody><thead><tr><th colspan="5"><div><span class="rhi"><i class="icofont-brainstorming icofont-2x"></i></span><span class="rh">Impacto sobre la salud mental</span></th></tr></thead><tbody>$psychological_effects</tbody><thead><tr><th colspan="5"><div><span class="rhi"><i class="icofont-bed icofont-2x"></i></span><span class="rh">Impacto sobre la calidad del sueño</span></th></tr></thead><tbody>$sleep_effects</tbody></table>',
            st = v =>{
                if (v == void 0 || isNaN(v)){
                    return '> -';
                }
                let 
                    bgi = vega.interpolateColors(["#008080","#8aacaf","#ffffff","#fb90b6","#ff1d8e"], 'lab'),
                    fri = vega.interpolateColors(["#008080","#4a7373","#666666","#b5587a","#ff1d8e"], 'lab'),
                    n = Math.min(Math.max(v, -100), 100),
                    nc = (100 + n) / 200,
                    nf = 400 + 5 * Math.floor(Math.abs(n)),
                    bg = bgi(nc).replace(')',', 0.2)'),
                    fr = fri(nc),
                    cl = ` style="color: ${fr}; background: ${bg}; font-weight: ${nf}">${v}`;
                return cl;
            };
            for (const dis in madb.data.risks) {
                const d = madb.data.risks[dis];
                let body = '';
                for(const r of d){
                    let 
                        z = zona[r.param],
                        m = madrid[r.param],
                        aa = f(r,z,'x'),
                        bb = f(r,m,'x'),
                        cc = f(r,z,'n'),
                        dd = f(r,m,'n'),
                        row = `<tr><td class="dsc">${r.desc}</td><td${st(aa)}%</td><td${st(bb)}%</td><td${st(cc)}%</td><td${st(dd)}%</td></tr>`;
                    body += row;
                }   
                rcontent = rcontent.replace(`$${dis}`, body);
            }
            document.querySelector('.risks').innerHTML = rcontent;
        },
        getbuilding = e =>{            
            const 
                features = e.target.queryRenderedFeatures(e.point).filter(k=>k.source=="buildings");
            let
                summary = document.querySelector('.fsummary'),
                nsummary = document.querySelector('.nsummary'),
                rheader = document.querySelector('.rheader'),
                risks = document.querySelector('.risks');            
            document.querySelectorAll('.mapboxgl-popup').forEach(k => k.remove());
            if(features.length == 0 || madb.z < 15) {
                nsummary.innerHTML ='';
                rheader.innerHTML ='';
                risks.innerHTML ='';
                madb.data.building = void 0;
                madb.data.floor = {f:0};                
                if( madb.z < 15){
                    summary.innerHTML='<h1 class="as-title"><span class="as-info as-font--medium">Acércate en el mapa</span> y haz click en un edificio para ver su información detallada</h1>';
                }else{
                    summary.innerHTML='<h1 class="as-title">Haz click en un edificio para ver su información detallada</h1>';
                }
                return false;
            }
            const 
                uses = {
                    'agriculture': 'Suelo de uso <span class="as-info as-font--medium">agrícola</span>',
                    'industrial': 'Edificio de uso <span class="as-info as-font--medium">industrial</span>',
                    'office': 'Edificio de <span class="as-info as-font--medium">oficinas</span>',
                    'publicServices': 'Edificio de <span class="as-info as-font--medium">equipamiento</span>',
                    'residential': 'Edificio <span class="as-info as-font--medium">residencial</span>',
                    'retail': 'Edificio <span class="as-info as-font--medium">comercial</span>'
                },
                usesicons = {
                    'agriculture': '<i title="Agrícola" class="icofont-wheat icofont-2x"></i>',
                    'industrial': '<i title="Industrial" class="icofont-industries-5 icofont-2x"></i>',
                    'office': '<i title="Oficinas" class="icofont-ui-office icofont-2x"></i>',
                    'publicServices': '<i title="Equipamiento" class="icofont-building-alt icofont-2x"></i>',
                    'residential': '<i title="Residencial" class="icofont-home icofont-2x"></i>',
                    'retail': '<i title="Comercial" class="icofont-shopping-cart icofont-2x"></i>'
                },               
                f = features[0].properties;
            madb.data.building = f;
            let
                nodata = f.ld_mx == void 0,
                rf=[f.reference.substring(0,7),f.reference.substr(f.reference.length - 7)],
                useicon =`<div>${usesicons[f.use]}</div>`,
               scontent = `
                <p class="as-subheader">
                ${uses[f.use]}${(f.floors !== void 0)?` de <span class='as-info as-font--medium'>${f.floors}</span> alturas`:''}${(f.nviv>0)?`, con <span class='as-info as-font--medium'>${f.nviv}
                </span> vivienda${(f.nviv==1)?'':'s'},`:''} ${(f.use=='agriculture')? '':` construido en el año 
                <span class='as-info as-font--medium'>${f.age}`}</span> 
                y con ref. catastral <span class='as-font--medium as-ref as-help'>${f.reference} <img src="assets/img/cadaster.png"></span>.
                </p>
                ${(nodata || f.floors == void 0 || f.floors < 2)? '':`<p class="nope">
                    <span class="as-subheader">Elige la planta a estudiar (el 0 es el BAJO): </span>
                    <input class="as-input fselector" type="number" min="0" max="${f.floors - 1}" step="1" value="0" oninput="madb.getrisks()">
                    <i class="icofont-info-circle plantas"></i>
                </p> `}
                `; 

            new mapboxgl.Popup({closeButton:false, className: 'classday'})
                .setLngLat(e.lngLat)
                .setHTML(useicon)
                .addTo(madb.day);
            new mapboxgl.Popup({closeButton:false, className: 'classnight'})
                .setLngLat(e.lngLat)
                .setHTML(useicon)
                .addTo(madb.night);
            summary.innerHTML = scontent;
            if (nodata){
                nsummary.innerHTML ='';
                risks.innerHTML ='';
                rheader.innerHTML = '<p class="as-subheader">No hay información de ruido para este edificio</p>';
                return false;
            }
            getrisks();
            document.querySelector('.as-ref').addEventListener('click', e=>{
                _showinfo(` <div class="container-loader"><div class="loader is-visible"></div></div>
                    <iframe class="dcontent refcat" src="https://www1.sedecatastro.gob.es/CYCBienInmueble/OVCListaBienes.aspx?RC1=${rf[0]}&RC2=${rf[1]}">
                    </iframe>`);
                document.querySelector('.refcat').addEventListener('load', e => {
                    document.querySelector('dialog .loader').classList.remove('is-visible');
                });
                e.stopPropagation();
                e.preventDefault();
            }, false);
            _addinfo('.plantas', '.info-plantas');
        },
        breath = s =>{
            const phs = document.querySelectorAll(s);
            phs.forEach(ph=>{
                const t = ph.textContent;
                ph.innerHTML = '';
                ph.append(...[...t].map((l, i) => {
                    const n = document.createElement('span');
                    n.textContent = l;
                    n.style.display = 'inline-block';
                    return n;
                }));
            });
        },
        init = () => {   
            //#region MAP INIT
            madb.day = new mapboxgl.Map({
                container: 'day',
                style: madb.style.day,
                antialias: true,
                minZoom: 11,
                maxZoom: 19,
                maxBounds: [
                    [-3.883845, 40.318194],
                    [-3.518623, 40.636983]
                ]
            });

            madb.night = new mapboxgl.Map({
                container: 'night',
                style: madb.style.night,
                antialias: true,
                minZoom: 11,
                maxZoom: 19,
                maxBounds: [
                    [-3.883845, 40.318194],
                    [-3.518623, 40.636983]
                ]
            });
            madb.night.addControl(new mapboxgl.NavigationControl(), 'bottom-right');
            document.querySelector('.zoomcontrol').appendChild(document.querySelector('.mapboxgl-ctrl-group'));
            
            madb.map = new mapboxgl.Compare(madb.day, madb.night, {});
            //#endregion

            //#region MAP EVENTS
            const spin = () =>{
                let spinners = document.querySelectorAll('.container-loader .loader');
                spinners.forEach(k => { k.classList.add('is-visible'); });
            };
            madb.day.on('load', e => {
                madb.day.on('idle', newdata);
                newdata(e);
            });
            madb.day.on('movestart', spin);
            madb.day.on('boxzoomstart', spin);
            madb.day.on('zoomend', e =>{
                if (!madb.night.isZooming()) pitcher(e.target);
            });
            madb.night.on('zoomend', e =>{
                if (!madb.day.isZooming()) pitcher(e.target);
            });
            madb.day.on('click', getbuilding);
            madb.night.on('click', getbuilding);
            madb.day.on('error', e => console.log(e.error));
            madb.night.on('error', e => console.log(e.error));
            document.querySelector('#switcher-1').addEventListener('change', e => {
                madb.style.exposed = e.target.checked;
                madb.style.change();
            });
            document.querySelector('#switcher-2').addEventListener('change', e => {
                madb.style.inner = !e.target.checked;
                madb.style.change();
            });
            //#endregion

            //#region DIALOGS
            madb.dialog = document.createElement('dialog');
            dialogPolyfill.registerDialog(madb.dialog);
            window.addEventListener('click', function(e){
                let a = document.querySelector('dialog .dcontent');
                if (a == void 0 || !a.contains(e.target)){
                  madb.dialog.close();
                }
              });
            document.body.appendChild(madb.dialog);
            _addinfo('.residesc', '.info-residesc');
            _addinfo('#vis1', '.info-vis1');
            _addinfo('.noisedesc', '.info-noisedesc');
            _addinfo('#vis2', '.info-vis2');
            _addinfo('.as-yo', '.info-yo', '.egoname');
            _addinfo('.sharer', '.info-share');
            _addinfo('.method', '.info-method');
            _addinfo('.attrib', '.info-attrib');
            _addinfo('.justi', '.info-justi');
            //#endregion

            document.querySelector('.as-search').addEventListener('click', _lookup);
            document.querySelector('#lookup').addEventListener('keyup', function(e) {
                if (e.keyCode === 13) {
                  e.preventDefault();
                  _lookup();
                }
            });

            if ('serviceWorker' in navigator) {
                navigator.serviceWorker.register('assets/scripts/sw.js');
            }

        };

    madb.init = init;
    madb.getrisks = getrisks;

})();