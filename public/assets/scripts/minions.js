/*jshint esversion: 6 */
(function () {

    'use strict';

    window.madb.minions = window.madb.minions || {};

    const app_path = location.href.replace(/[^/]*$/, '');

    window.minion = function (f, onmessage, imports, persist) {
        const
            imp = imports.reduce((acc,k)=>`importScripts('${k}');
            `,''),
            target = `
                ${imp}
                app_path = '${app_path}'
                onmessage = function (o){let _func = ${f.toString()};
                postMessage(_func.apply(null,o.data));
                ${(!persist)? 'self.close();}':'}'}`,
            mission = URL.createObjectURL(new Blob([target], {'type': 'text/javascript'})),
            minion = new Worker(mission);
            minion.onmessage = onmessage;
        return minion;
    };

    const
        stats_mission = e => dl.summary(e.data, e.fields),
        stats_callback = e => {
            let                 
                loaderspinner = document.querySelector('.as-spinner'),               
                spinners = document.querySelectorAll('.container-loader .loader'),
                nviv = Object.keys(e.data[2].unique).reduce((a,b) => a + b * e.data[2].unique[b], 0),
                pcresi = Math.round(100 * (e.data[1].unique.residential || 0) / e.data[1].count),
                den = e.data[6],
                d = e.data[3],
                n = e.data[5],
                resimsg = `<ul>
                    <li>Zona <span class='as-info as-font--medium'>${pcresi}%</span> residencial</li>
                    <li>Un total de <span class='as-info as-font--medium'>${nviv}</span> vivienda${(nviv==1)?'':'s'}</li>
                    <li>Zona consolidada antes de <span class='as-info as-font--medium'>${Math.round(e.data[0].q3)}</span></li>
                    </ul>`,
                nm =[],
                noisemsg='';

            nm[0] = (den.median>=73)? den.median - 3 : void 0;
            nm[1] = (den.max>=73)? den.max - 3 : void 0;
            nm[2] = (den.median>=56)? den.median - 3 : void 0;
            nm[3] = (n.median>=48)? n.median - 3 : void 0;
            nm[4] = (den.max>=56)? den.max - 3 : void 0;
            nm[5] = (n.max>=48)? n.max - 3 : void 0;

            if(!!nm[0]){
                noisemsg=`<span class="as-hot">Esta zona sufre un caso severo de contaminación acústica con un valor central de ${nm[0]}dB L<sub>den</sub>.</span>`;
            }else{
                if(!!nm[1]){
                    noisemsg=`<span class="as-hot">Esta zona tiene algunos edificios expuestos a niveles severos de contaminación acústica, llegando a ${nm[1]}dB L<sub>den</sub></span>, `;
                }
                const b = noisemsg.length>0;
                if(!!nm[2] && !!nm[3]){
                    noisemsg += b? ' y además <span class="as-warm">tanto ':'<span class="as-warm">Tanto ';
                    noisemsg += `los niveles medios 24h (${nm[2]}dB) como nocturnos (${nm[3]}dB) superan los límites recomendados</span> (53dB diurno, 45dB nocturno).`;
                }else if(!!nm[2] && !nm[3]){
                    noisemsg += b? ' y además <span class="as-warm">los ':'<span class="as-warm">Los ';
                    noisemsg += `niveles medios 24h (${nm[2]}dB) superan el límite recomendado</span> (53dB).`;
                }else if(!nm[2] && !!nm[3]){
                    noisemsg += b? ' y además <span class="as-warm">los ':'<span class="as-warm">Los ';
                    noisemsg += `niveles medios nocturnos (${nm[3]}dB) superan el límite recomendado</span> (45dB).`;
                }else{
                    noisemsg += b? ' pero <span class="as-cold">tanto ':'<span class="as-cold">Tanto ';
                    noisemsg += `los niveles medios 24h (${den.median - 3}dB) como nocturnos (${n.median - 3}dB) están dentro de los límites recomendados.</span>`;
                }   
            } 

            madb.data.summary = e.data;
            document.querySelector('.residesc').innerHTML = resimsg;
            document.querySelector('.noisedesc').innerHTML = noisemsg;

            madb.data.histogram('#vis1',{x: 'age', xlabel:'Año de construcción'});
            madb.data.heatmap('#vis2',{
                x:'lden_mx', 
                xlabel: '24h (dB)',
                y:'ln_mx',
                ylabel: 'Nocturno (dB)',
                title: 'Distribución de edificios por exposición al ruido'
            });
            spinners.forEach(k=>{k.classList.remove('is-visible');});
            if (loaderspinner.style.opacity !== '0'){
                document.querySelectorAll('.as-switch__label').forEach(k => {
                    k.classList.remove('as-body'); 
                    k.classList.add('as-subheader'); 
                });
                loaderspinner.style.opacity = 0;
                loaderspinner.style.pointerEvents='none';
                console.log(`MAdB was loaded in`);
                console.timeEnd('main');
            }
            if(madb.data.building != void 0){
                madb.getrisks();
            }
        };
    madb.minions.stats = minion(stats_mission, stats_callback,[`${app_path}assets/scripts/datalib.js`], true);

    madb.minions.lookup = new Worker("assets/scripts/callejero.js");
    madb.minions.lookup.onmessage = e => {
        const
        _median = o => {
            const 
                m = Math.floor(o.length / 2),
                n = [...o].sort((a, b) => a - b);
            return o.length % 2 !== 0 ? n[m] : (n[m - 1] + n[m]) / 2;
          },
        disclaimer =`<p class="as-body as-color--type-02">
            Si no encuentras los resultados deseados, prueba a añadir el <span class='as-info'>tipo de vía</span> (calle, plaza, avenida, etc.) y/o el <span class='as-info'>número</span>.
        </p>`;
        let 
            results,
            r = e.data;
        if (r.length == 0){
            results = '<h3 class="as-title">No se han encontrado resultados con los términos de búsqueda</h3>';
            results += disclaimer;
            if (!!madb.dialog.open) madb.dialog.close();
            madb.dialog.innerHTML = results;madb.dialog.innerHTML = t;
            if (!!!madb.dialog.open) madb.dialog.showModal();
        }else if(r.length == 1 || r[0].score == 0){
            madb.day.flyTo({center: [r[0].item.x, r[0].item.y], zoom: 17});
        }else{
            const
                cs = ["#ff1d8e", "#e0448b", "#c25689", "#a16287", "#7f6984", "#546e82", "#007080"];
            results ='<h3 class="as-title">Resultados más cercanos a la búsqueda</h3><ul>';            
            r.forEach(o =>{
                const 
                    k = o.item,
                    i= Math.max(Math.min(Math.floor((o.score * 25)) - 1, 6), 0),
                    c = cs[i];
                results +='<li>';
                results +=`<span class="as-subheader as-address" style="color:${c}" data-x="${k.x}" data-y="${k.y}">${k.n}</span>`;
                results +='</li>';
            });
            results +='</ul><div class="dial-scale"><span class="as-body as-color--type-02">Los resultados se colorean de menos a mas precisos según </span> <span class="scale"></span> </div>';
            results += disclaimer;
            
            if (madb.dialog.open) madb.dialog.close();
            madb.dialog.innerHTML = results;
            document.querySelectorAll('.as-address').forEach(k=>{
                k.addEventListener('click', e =>{
                    madb.day.flyTo({center: [k.dataset.x, k.dataset.y], zoom: 17});
                })
            })
            madb.dialog.showModal();  
        }
        document.querySelector('#lookup').classList.remove('searching');
    };

})();