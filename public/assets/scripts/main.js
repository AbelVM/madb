/*jshint esversion: 6 */
(function () {
    'use strict';

    // no tiene sentido ver esta app en un móvil :(
    if (screen.width < 1280 || screen.height < 740) {
        document.location = "mobile.html";
    }

    console.time('main');

    //#region ENVIRONMENT
    const
        appname = 'madb',
        version = {
            madb: 'v1.0.0',
            mapbox: 'v1.6.1',
            compare: 'v0.1.1',
            airship: 'v2.1.1',
            vega:[5,4,6,'1.9.2']
        },
        resources={
            css:[
                /* `https://libs.cartocdn.com/airship-style/${version.airship}/airship.css`,
                `https://api.tiles.mapbox.com/mapbox-gl-js/${version.mapbox}/mapbox-gl.css`,
                `https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-compare/${version.compare}/mapbox-gl-compare.css`,
                `https://libs.cartocdn.com/airship-icons/${version.airship}/icons.css`, */
                `assets/styles/airship.css`,
                `assets/styles/mapbox-gl.css`,
                `assets/styles/mapbox-gl-compare.css`,
                'assets/styles/dialog-polyfill.css',
                'assets/icofont/icofont.min.css',
                'assets/styles/madb.css'
            ],
            jscore:[
               'assets/scripts/dialog-polyfill.js', 
               /*  `https://libs.cartocdn.com/airship-components/${version.airship}/airship.js`,
                `https://api.tiles.mapbox.com/mapbox-gl-js/${version.mapbox}/mapbox-gl.js`,
                `https://cdn.jsdelivr.net/npm/vega@${version.vega[0]}`,
                `https://cdn.jsdelivr.net/npm/vega-lite@${version.vega[1]}`,
                `https://cdn.jsdelivr.net/npm/datalib/datalib.min.js`, */
                //`assets/scripts/airship.js`,
                `assets/scripts/mapbox-gl.js`,
                `assets/scripts/vega.js`,
                `assets/scripts/vega-lite.js`,
                'assets/scripts/stats.js',
                'assets/scripts/layers.js',
                'assets/scripts/minions.js'
            ] ,
            js:[
               /*  `https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-compare/${version.compare}/mapbox-gl-compare.js`,                
                `https://cdn.jsdelivr.net/npm/vega-embed@${version.vega[2]}`, */
                `assets/scripts/mapbox-gl-compare.js`,                
                `assets/scripts/vega-embed.js`,
                'assets/scripts/madb.js'/*,
                'assets/scripts/premonish.js'*/
            ]
        };
    //#endregion

    //#region IMPORT
    const
        embed ={
            js:{
                url: (url, wait = false, defer = true) => {
                    return new Promise((resolve, reject) => {
                        let s = document.createElement('script');
                        s.type = 'text/javascript';
                        s.src = url;
                        s.defer = !!defer;
                        s.async = !!!defer;
                        s.addEventListener('error', () => reject(s), false);                        
                        if(wait=== true){                            
                            s.addEventListener('load', () => resolve(s), false);
                            document.head.appendChild(s);
                        }else{
                            document.head.appendChild(s);
                            resolve(s);
                        }
                    });
                },
                urls: (urls, wait = true, defer = true) => {
                    return Promise.all(urls.map(url=>embed.js.url(url, wait, defer)));
                },
                content: (code, defer = false) => {
                    return new Promise((resolve, reject) => {
                        let s = document.createElement('script');
                        s.type = 'text/javascript';
                        s.innerText = code;    
                        s.defer = defer;
                        document.head.appendChild(s);
                        resolve(s);
                    });
                }
            },
            css:{
                url: (url) => {
                    return new Promise((resolve, reject) => {
                        let style = document.createElement('link');
                        style.rel = 'stylesheet';
                        style.href = url;
                        document.head.appendChild(style);
                        resolve(style);
                    });
                },
                urls: (urls) => {
                    return Promise.all(urls.map(embed.css.url));
                }
            }
        },
        init = () => {
            embed.css.urls(resources.css);    
            embed.js.urls(resources.jscore, false)
            .then(() =>
                embed.js.urls(resources.js, true)
            )
            .then(() => {
                if (document.readyState === 'complete') {
                    madb.init();
                } else {
                    document.addEventListener("readystatechange", madb.init);
                }
                    document.addEventListener("readystatechange", madb.init);                
            });
        };
    //#endregion

    init();

})();