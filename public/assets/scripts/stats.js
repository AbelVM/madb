/*jshint esversion: 6 */
(function () {

  'use strict';

  window.madb = window.madb || {};

  window.madb.data = window.madb.data || {};

  madb.data.fields = ['age', 'use', 'nviv', 'ld_mx', 'le_mx', 'ln_mx', 'lden_mx', 'ld_mn', 'le_mn', 'ln_mn', 'lden_mn', 'legal_sleep', 'legal_live', 'sleep_mx', 'live_mx', 'sleep_mn', 'live_mn', 'not_cam_night', 'not_cam_day', 'not_eu_night', 'not_eu_day', 'severe_eu'];

  madb.data.global = {
    'age': { 'median': 1967, 'breaks': { 'lower': [1605, 1940, 1958, 1964, 1970, 1981, 1996], 'higher': [1940, 1958, 1964, 1970, 1981, 1996, 2019] }, 'histogram': [{ 'bin_start': 1605, 'bin_end': 1613, 'count': 1 }, { 'bin_start': 1638, 'bin_end': 1646, 'count': 1 }, { 'bin_start': 1704, 'bin_end': 1713, 'count': 2 }, { 'bin_start': 1713, 'bin_end': 1721, 'count': 1 }, { 'bin_start': 1746, 'bin_end': 1754, 'count': 5 }, { 'bin_start': 1754, 'bin_end': 1762, 'count': 1 }, { 'bin_start': 1762, 'bin_end': 1771, 'count': 5 }, { 'bin_start': 1779, 'bin_end': 1787, 'count': 3 }, { 'bin_start': 1787, 'bin_end': 1795, 'count': 3 }, { 'bin_start': 1795, 'bin_end': 1804, 'count': 13 }, { 'bin_start': 1804, 'bin_end': 1812, 'count': 1 }, { 'bin_start': 1812, 'bin_end': 1820, 'count': 18 }, { 'bin_start': 1820, 'bin_end': 1829, 'count': 1 }, { 'bin_start': 1829, 'bin_end': 1837, 'count': 2 }, { 'bin_start': 1837, 'bin_end': 1845, 'count': 5 }, { 'bin_start': 1845, 'bin_end': 1853, 'count': 129 }, { 'bin_start': 1853, 'bin_end': 1862, 'count': 137 }, { 'bin_start': 1862, 'bin_end': 1870, 'count': 23 }, { 'bin_start': 1870, 'bin_end': 1878, 'count': 111 }, { 'bin_start': 1878, 'bin_end': 1887, 'count': 636 }, { 'bin_start': 1887, 'bin_end': 1895, 'count': 390 }, { 'bin_start': 1895, 'bin_end': 1903, 'count': 3422 }, { 'bin_start': 1903, 'bin_end': 1911, 'count': 1313 }, { 'bin_start': 1911, 'bin_end': 1920, 'count': 902 }, { 'bin_start': 1920, 'bin_end': 1928, 'count': 3107 }, { 'bin_start': 1928, 'bin_end': 1936, 'count': 4434 }, { 'bin_start': 1936, 'bin_end': 1944, 'count': 4682 }, { 'bin_start': 1944, 'bin_end': 1953, 'count': 7313 }, { 'bin_start': 1953, 'bin_end': 1961, 'count': 21539 }, { 'bin_start': 1961, 'bin_end': 1969, 'count': 18902 }, { 'bin_start': 1969, 'bin_end': 1978, 'count': 15152 }, { 'bin_start': 1978, 'bin_end': 1986, 'count': 9674 }, { 'bin_start': 1986, 'bin_end': 1994, 'count': 10041 }, { 'bin_start': 1994, 'bin_end': 2002, 'count': 9687 }, { 'bin_start': 2002, 'bin_end': 2011, 'count': 7515 }, { 'bin_start': 2011, 'bin_end': 2019, 'count': 2389 }, { 'bin_start': 2019, 'bin_end': 2019, 'count': 52 }] },
    'nviv': { 'median': 5, 'breaks': { 'lower': [0, 1, 1, 1, 8, 12, 23], 'higher': [1, 1, 1, 8, 12, 23, 1473] }, 'histogram': [{ 'bin_start': 0, 'bin_end': 29, 'count': 109737 }, { 'bin_start': 29, 'bin_end': 59, 'count': 7749 }, { 'bin_start': 59, 'bin_end': 88, 'count': 2012 }, { 'bin_start': 88, 'bin_end': 118, 'count': 867 }, { 'bin_start': 118, 'bin_end': 147, 'count': 576 }, { 'bin_start': 147, 'bin_end': 177, 'count': 285 }, { 'bin_start': 177, 'bin_end': 206, 'count': 140 }, { 'bin_start': 206, 'bin_end': 236, 'count': 97 }, { 'bin_start': 236, 'bin_end': 265, 'count': 42 }, { 'bin_start': 265, 'bin_end': 295, 'count': 36 }, { 'bin_start': 295, 'bin_end': 324, 'count': 25 }, { 'bin_start': 324, 'bin_end': 354, 'count': 19 }, { 'bin_start': 354, 'bin_end': 383, 'count': 12 }, { 'bin_start': 383, 'bin_end': 412, 'count': 6 }, { 'bin_start': 412, 'bin_end': 442, 'count': 4 }, { 'bin_start': 442, 'bin_end': 471, 'count': 3 }, { 'bin_start': 471, 'bin_end': 501, 'count': 4 }, { 'bin_start': 501, 'bin_end': 530, 'count': 1 }, { 'bin_start': 530, 'bin_end': 560, 'count': 1 }, { 'bin_start': 619, 'bin_end': 648, 'count': 1 }, { 'bin_start': 707, 'bin_end': 737, 'count': 1 }, { 'bin_start': 1473, 'bin_end': 1473, 'count': 1 }] },
    'ld_mx': { 'median': 57, 'breaks': { 'lower': [-5, 52, 55, 57, 58, 60, 63], 'higher': [52, 55, 57, 58, 60, 63, 78] }, 'histogram': [{ 'bin_start': -5, 'bin_end': -3, 'count': 1 }, { 'bin_start': 3, 'bin_end': 5, 'count': 3 }, { 'bin_start': 7, 'bin_end': 8, 'count': 1 }, { 'bin_start': 8, 'bin_end': 10, 'count': 1 }, { 'bin_start': 10, 'bin_end': 12, 'count': 1 }, { 'bin_start': 13, 'bin_end': 15, 'count': 2 }, { 'bin_start': 15, 'bin_end': 17, 'count': 3 }, { 'bin_start': 17, 'bin_end': 18, 'count': 1 }, { 'bin_start': 18, 'bin_end': 20, 'count': 4 }, { 'bin_start': 22, 'bin_end': 23, 'count': 4 }, { 'bin_start': 23, 'bin_end': 25, 'count': 1 }, { 'bin_start': 25, 'bin_end': 27, 'count': 4 }, { 'bin_start': 27, 'bin_end': 28, 'count': 9 }, { 'bin_start': 28, 'bin_end': 30, 'count': 6 }, { 'bin_start': 30, 'bin_end': 32, 'count': 20 }, { 'bin_start': 32, 'bin_end': 33, 'count': 23 }, { 'bin_start': 33, 'bin_end': 35, 'count': 10 }, { 'bin_start': 35, 'bin_end': 37, 'count': 17 }, { 'bin_start': 37, 'bin_end': 38, 'count': 24 }, { 'bin_start': 38, 'bin_end': 40, 'count': 17 }, { 'bin_start': 40, 'bin_end': 41, 'count': 144 }, { 'bin_start': 41, 'bin_end': 43, 'count': 448 }, { 'bin_start': 43, 'bin_end': 45, 'count': 405 }, { 'bin_start': 45, 'bin_end': 46, 'count': 1183 }, { 'bin_start': 46, 'bin_end': 48, 'count': 2134 }, { 'bin_start': 48, 'bin_end': 50, 'count': 1775 }, { 'bin_start': 50, 'bin_end': 51, 'count': 6395 }, { 'bin_start': 51, 'bin_end': 53, 'count': 11409 }, { 'bin_start': 53, 'bin_end': 55, 'count': 8025 }, { 'bin_start': 55, 'bin_end': 56, 'count': 19131 }, { 'bin_start': 56, 'bin_end': 58, 'count': 20490 }, { 'bin_start': 58, 'bin_end': 60, 'count': 9330 }, { 'bin_start': 60, 'bin_end': 61, 'count': 15454 }, { 'bin_start': 61, 'bin_end': 63, 'count': 10676 }, { 'bin_start': 63, 'bin_end': 65, 'count': 3626 }, { 'bin_start': 65, 'bin_end': 66, 'count': 4853 }, { 'bin_start': 66, 'bin_end': 68, 'count': 2869 }, { 'bin_start': 68, 'bin_end': 70, 'count': 988 }, { 'bin_start': 70, 'bin_end': 71, 'count': 1233 }, { 'bin_start': 71, 'bin_end': 73, 'count': 549 }, { 'bin_start': 73, 'bin_end': 75, 'count': 108 }, { 'bin_start': 75, 'bin_end': 76, 'count': 74 }, { 'bin_start': 76, 'bin_end': 78, 'count': 2 }, { 'bin_start': 78, 'bin_end': 78, 'count': 1 }] },
    'le_mx': { 'median': 57, 'breaks': { 'lower': [-7, 52, 54, 56, 58, 59, 62], 'higher': [52, 54, 56, 58, 59, 62, 77] }, 'histogram': [{ 'bin_start': -7, 'bin_end': -5, 'count': 1 }, { 'bin_start': 1, 'bin_end': 3, 'count': 3 }, { 'bin_start': 3, 'bin_end': 5, 'count': 1 }, { 'bin_start': 5, 'bin_end': 6, 'count': 1 }, { 'bin_start': 8, 'bin_end': 10, 'count': 1 }, { 'bin_start': 10, 'bin_end': 11, 'count': 1 }, { 'bin_start': 11, 'bin_end': 13, 'count': 2 }, { 'bin_start': 13, 'bin_end': 15, 'count': 1 }, { 'bin_start': 15, 'bin_end': 17, 'count': 1 }, { 'bin_start': 17, 'bin_end': 18, 'count': 3 }, { 'bin_start': 18, 'bin_end': 20, 'count': 2 }, { 'bin_start': 22, 'bin_end': 23, 'count': 3 }, { 'bin_start': 23, 'bin_end': 25, 'count': 1 }, { 'bin_start': 25, 'bin_end': 27, 'count': 6 }, { 'bin_start': 27, 'bin_end': 28, 'count': 11 }, { 'bin_start': 28, 'bin_end': 30, 'count': 7 }, { 'bin_start': 30, 'bin_end': 32, 'count': 21 }, { 'bin_start': 32, 'bin_end': 33, 'count': 28 }, { 'bin_start': 33, 'bin_end': 35, 'count': 7 }, { 'bin_start': 35, 'bin_end': 37, 'count': 19 }, { 'bin_start': 37, 'bin_end': 38, 'count': 17 }, { 'bin_start': 38, 'bin_end': 40, 'count': 82 }, { 'bin_start': 40, 'bin_end': 42, 'count': 162 }, { 'bin_start': 42, 'bin_end': 43, 'count': 564 }, { 'bin_start': 43, 'bin_end': 45, 'count': 1054 }, { 'bin_start': 45, 'bin_end': 47, 'count': 833 }, { 'bin_start': 47, 'bin_end': 48, 'count': 2843 }, { 'bin_start': 48, 'bin_end': 50, 'count': 5826 }, { 'bin_start': 50, 'bin_end': 52, 'count': 4592 }, { 'bin_start': 52, 'bin_end': 53, 'count': 13420 }, { 'bin_start': 53, 'bin_end': 55, 'count': 18336 }, { 'bin_start': 55, 'bin_end': 57, 'count': 10545 }, { 'bin_start': 57, 'bin_end': 59, 'count': 20090 }, { 'bin_start': 59, 'bin_end': 60, 'count': 16199 }, { 'bin_start': 60, 'bin_end': 62, 'count': 6551 }, { 'bin_start': 62, 'bin_end': 64, 'count': 8975 }, { 'bin_start': 64, 'bin_end': 65, 'count': 5155 }, { 'bin_start': 65, 'bin_end': 67, 'count': 1656 }, { 'bin_start': 67, 'bin_end': 69, 'count': 2297 }, { 'bin_start': 69, 'bin_end': 70, 'count': 1293 }, { 'bin_start': 70, 'bin_end': 72, 'count': 378 }, { 'bin_start': 72, 'bin_end': 74, 'count': 348 }, { 'bin_start': 74, 'bin_end': 75, 'count': 108 }, { 'bin_start': 75, 'bin_end': 77, 'count': 6 }, { 'bin_start': 77, 'bin_end': 77, 'count': 2 }] },
    'ln_mx': { 'median': 52, 'breaks': { 'lower': [-10, 47, 50, 51, 53, 55, 58], 'higher': [47, 50, 51, 53, 55, 58, 74] }, 'histogram': [{ 'bin_start': -10, 'bin_end': -8, 'count': 1 }, { 'bin_start': -3, 'bin_end': -2, 'count': 2 }, { 'bin_start': -2, 'bin_end': 0, 'count': 1 }, { 'bin_start': 0, 'bin_end': 2, 'count': 1 }, { 'bin_start': 3, 'bin_end': 5, 'count': 2 }, { 'bin_start': 8, 'bin_end': 10, 'count': 4 }, { 'bin_start': 12, 'bin_end': 14, 'count': 2 }, { 'bin_start': 14, 'bin_end': 15, 'count': 4 }, { 'bin_start': 17, 'bin_end': 19, 'count': 2 }, { 'bin_start': 19, 'bin_end': 20, 'count': 4 }, { 'bin_start': 20, 'bin_end': 22, 'count': 1 }, { 'bin_start': 22, 'bin_end': 24, 'count': 9 }, { 'bin_start': 24, 'bin_end': 25, 'count': 12 }, { 'bin_start': 25, 'bin_end': 27, 'count': 12 }, { 'bin_start': 27, 'bin_end': 29, 'count': 20 }, { 'bin_start': 29, 'bin_end': 30, 'count': 23 }, { 'bin_start': 30, 'bin_end': 32, 'count': 9 }, { 'bin_start': 32, 'bin_end': 34, 'count': 26 }, { 'bin_start': 34, 'bin_end': 35, 'count': 21 }, { 'bin_start': 35, 'bin_end': 37, 'count': 110 }, { 'bin_start': 37, 'bin_end': 39, 'count': 147 }, { 'bin_start': 39, 'bin_end': 40, 'count': 759 }, { 'bin_start': 40, 'bin_end': 42, 'count': 1721 }, { 'bin_start': 42, 'bin_end': 44, 'count': 1353 }, { 'bin_start': 44, 'bin_end': 45, 'count': 4894 }, { 'bin_start': 45, 'bin_end': 47, 'count': 9812 }, { 'bin_start': 47, 'bin_end': 49, 'count': 7255 }, { 'bin_start': 49, 'bin_end': 50, 'count': 18240 }, { 'bin_start': 50, 'bin_end': 52, 'count': 21131 }, { 'bin_start': 52, 'bin_end': 54, 'count': 10009 }, { 'bin_start': 54, 'bin_end': 56, 'count': 16520 }, { 'bin_start': 56, 'bin_end': 57, 'count': 11871 }, { 'bin_start': 57, 'bin_end': 59, 'count': 4276 }, { 'bin_start': 59, 'bin_end': 61, 'count': 5607 }, { 'bin_start': 61, 'bin_end': 62, 'count': 3436 }, { 'bin_start': 62, 'bin_end': 64, 'count': 1236 }, { 'bin_start': 64, 'bin_end': 66, 'count': 1649 }, { 'bin_start': 66, 'bin_end': 67, 'count': 840 }, { 'bin_start': 67, 'bin_end': 69, 'count': 229 }, { 'bin_start': 69, 'bin_end': 71, 'count': 180 }, { 'bin_start': 71, 'bin_end': 72, 'count': 21 }, { 'bin_start': 72, 'bin_end': 74, 'count': 1 }, { 'bin_start': 74, 'bin_end': 74, 'count': 1 }] },
    'lden_mx': { 'median': 60, 'breaks': { 'lower': [-2, 55, 58, 59, 61, 63, 66], 'higher': [55, 58, 59, 61, 63, 66, 81] }, 'histogram': [{ 'bin_start': -2, 'bin_end': 0, 'count': 1 }, { 'bin_start': 5, 'bin_end': 6, 'count': 1 }, { 'bin_start': 6, 'bin_end': 8, 'count': 2 }, { 'bin_start': 10, 'bin_end': 11, 'count': 1 }, { 'bin_start': 11, 'bin_end': 13, 'count': 1 }, { 'bin_start': 13, 'bin_end': 15, 'count': 1 }, { 'bin_start': 16, 'bin_end': 18, 'count': 2 }, { 'bin_start': 18, 'bin_end': 20, 'count': 2 }, { 'bin_start': 20, 'bin_end': 21, 'count': 2 }, { 'bin_start': 21, 'bin_end': 23, 'count': 4 }, { 'bin_start': 25, 'bin_end': 26, 'count': 2 }, { 'bin_start': 26, 'bin_end': 28, 'count': 4 }, { 'bin_start': 28, 'bin_end': 30, 'count': 1 }, { 'bin_start': 30, 'bin_end': 31, 'count': 9 }, { 'bin_start': 31, 'bin_end': 33, 'count': 9 }, { 'bin_start': 33, 'bin_end': 35, 'count': 15 }, { 'bin_start': 35, 'bin_end': 36, 'count': 21 }, { 'bin_start': 36, 'bin_end': 38, 'count': 12 }, { 'bin_start': 38, 'bin_end': 40, 'count': 19 }, { 'bin_start': 40, 'bin_end': 41, 'count': 26 }, { 'bin_start': 41, 'bin_end': 43, 'count': 8 }, { 'bin_start': 43, 'bin_end': 44, 'count': 65 }, { 'bin_start': 44, 'bin_end': 46, 'count': 300 }, { 'bin_start': 46, 'bin_end': 48, 'count': 325 }, { 'bin_start': 48, 'bin_end': 49, 'count': 1195 }, { 'bin_start': 49, 'bin_end': 51, 'count': 2165 }, { 'bin_start': 51, 'bin_end': 53, 'count': 1774 }, { 'bin_start': 53, 'bin_end': 54, 'count': 6641 }, { 'bin_start': 54, 'bin_end': 56, 'count': 11874 }, { 'bin_start': 56, 'bin_end': 58, 'count': 8224 }, { 'bin_start': 58, 'bin_end': 59, 'count': 19624 }, { 'bin_start': 59, 'bin_end': 61, 'count': 20869 }, { 'bin_start': 61, 'bin_end': 63, 'count': 9195 }, { 'bin_start': 63, 'bin_end': 64, 'count': 15100 }, { 'bin_start': 64, 'bin_end': 66, 'count': 10256 }, { 'bin_start': 66, 'bin_end': 68, 'count': 3442 }, { 'bin_start': 68, 'bin_end': 69, 'count': 4556 }, { 'bin_start': 69, 'bin_end': 71, 'count': 2804 }, { 'bin_start': 71, 'bin_end': 73, 'count': 959 }, { 'bin_start': 73, 'bin_end': 74, 'count': 1206 }, { 'bin_start': 74, 'bin_end': 76, 'count': 545 }, { 'bin_start': 76, 'bin_end': 78, 'count': 107 }, { 'bin_start': 78, 'bin_end': 79, 'count': 79 }, { 'bin_start': 79, 'bin_end': 81, 'count': 5 }, { 'bin_start': 81, 'bin_end': 81, 'count': 1 }] },
    'ld_mn': { 'median': 55, 'breaks': { 'lower': [-10, 49, 52, 54, 56, 58, 61], 'higher': [49, 52, 54, 56, 58, 61, 78] }, 'histogram': [{ 'bin_start': -10, 'bin_end': -8, 'count': 1 }, { 'bin_start': -6, 'bin_end': -5, 'count': 2 }, { 'bin_start': -5, 'bin_end': -3, 'count': 1 }, { 'bin_start': -1, 'bin_end': 1, 'count': 1 }, { 'bin_start': 2, 'bin_end': 4, 'count': 4 }, { 'bin_start': 6, 'bin_end': 8, 'count': 1 }, { 'bin_start': 8, 'bin_end': 9, 'count': 2 }, { 'bin_start': 9, 'bin_end': 11, 'count': 2 }, { 'bin_start': 13, 'bin_end': 15, 'count': 4 }, { 'bin_start': 15, 'bin_end': 16, 'count': 5 }, { 'bin_start': 16, 'bin_end': 18, 'count': 2 }, { 'bin_start': 18, 'bin_end': 20, 'count': 6 }, { 'bin_start': 20, 'bin_end': 22, 'count': 2 }, { 'bin_start': 22, 'bin_end': 23, 'count': 8 }, { 'bin_start': 23, 'bin_end': 25, 'count': 9 }, { 'bin_start': 25, 'bin_end': 27, 'count': 12 }, { 'bin_start': 27, 'bin_end': 29, 'count': 21 }, { 'bin_start': 29, 'bin_end': 30, 'count': 15 }, { 'bin_start': 30, 'bin_end': 32, 'count': 22 }, { 'bin_start': 32, 'bin_end': 34, 'count': 12 }, { 'bin_start': 34, 'bin_end': 36, 'count': 29 }, { 'bin_start': 36, 'bin_end': 38, 'count': 53 }, { 'bin_start': 38, 'bin_end': 39, 'count': 126 }, { 'bin_start': 39, 'bin_end': 41, 'count': 404 }, { 'bin_start': 41, 'bin_end': 43, 'count': 603 }, { 'bin_start': 43, 'bin_end': 45, 'count': 2914 }, { 'bin_start': 45, 'bin_end': 46, 'count': 5054 }, { 'bin_start': 46, 'bin_end': 48, 'count': 7582 }, { 'bin_start': 48, 'bin_end': 50, 'count': 4362 }, { 'bin_start': 50, 'bin_end': 52, 'count': 11065 }, { 'bin_start': 52, 'bin_end': 53, 'count': 14445 }, { 'bin_start': 53, 'bin_end': 55, 'count': 17056 }, { 'bin_start': 55, 'bin_end': 57, 'count': 8867 }, { 'bin_start': 57, 'bin_end': 59, 'count': 16216 }, { 'bin_start': 59, 'bin_end': 60, 'count': 12562 }, { 'bin_start': 60, 'bin_end': 62, 'count': 8816 }, { 'bin_start': 62, 'bin_end': 64, 'count': 2924 }, { 'bin_start': 64, 'bin_end': 66, 'count': 3952 }, { 'bin_start': 66, 'bin_end': 67, 'count': 2143 }, { 'bin_start': 67, 'bin_end': 69, 'count': 1190 }, { 'bin_start': 69, 'bin_end': 71, 'count': 371 }, { 'bin_start': 71, 'bin_end': 73, 'count': 424 }, { 'bin_start': 73, 'bin_end': 74, 'count': 136 }, { 'bin_start': 74, 'bin_end': 76, 'count': 27 }, { 'bin_start': 78, 'bin_end': 78, 'count': 1 }] },
    'le_mn': { 'median': 55, 'breaks': { 'lower': [-11, 48, 51, 54, 55, 58, 60], 'higher': [48, 51, 54, 55, 58, 60, 77] }, 'histogram': [{ 'bin_start': -11, 'bin_end': -9, 'count': 1 }, { 'bin_start': -9, 'bin_end': -7, 'count': 1 }, { 'bin_start': -7, 'bin_end': -6, 'count': 1 }, { 'bin_start': -6, 'bin_end': -4, 'count': 2 }, { 'bin_start': 1, 'bin_end': 3, 'count': 4 }, { 'bin_start': 3, 'bin_end': 5, 'count': 2 }, { 'bin_start': 5, 'bin_end': 7, 'count': 1 }, { 'bin_start': 8, 'bin_end': 10, 'count': 3 }, { 'bin_start': 10, 'bin_end': 12, 'count': 2 }, { 'bin_start': 12, 'bin_end': 14, 'count': 3 }, { 'bin_start': 14, 'bin_end': 15, 'count': 2 }, { 'bin_start': 15, 'bin_end': 17, 'count': 3 }, { 'bin_start': 17, 'bin_end': 19, 'count': 4 }, { 'bin_start': 19, 'bin_end': 21, 'count': 4 }, { 'bin_start': 21, 'bin_end': 22, 'count': 7 }, { 'bin_start': 22, 'bin_end': 24, 'count': 11 }, { 'bin_start': 24, 'bin_end': 26, 'count': 6 }, { 'bin_start': 26, 'bin_end': 28, 'count': 20 }, { 'bin_start': 28, 'bin_end': 29, 'count': 21 }, { 'bin_start': 29, 'bin_end': 31, 'count': 17 }, { 'bin_start': 31, 'bin_end': 33, 'count': 16 }, { 'bin_start': 33, 'bin_end': 35, 'count': 22 }, { 'bin_start': 35, 'bin_end': 37, 'count': 47 }, { 'bin_start': 37, 'bin_end': 38, 'count': 100 }, { 'bin_start': 38, 'bin_end': 40, 'count': 281 }, { 'bin_start': 40, 'bin_end': 42, 'count': 457 }, { 'bin_start': 42, 'bin_end': 44, 'count': 2373 }, { 'bin_start': 44, 'bin_end': 45, 'count': 4670 }, { 'bin_start': 45, 'bin_end': 47, 'count': 7017 }, { 'bin_start': 47, 'bin_end': 49, 'count': 4346 }, { 'bin_start': 49, 'bin_end': 51, 'count': 10532 }, { 'bin_start': 51, 'bin_end': 52, 'count': 13851 }, { 'bin_start': 52, 'bin_end': 54, 'count': 16724 }, { 'bin_start': 54, 'bin_end': 56, 'count': 8960 }, { 'bin_start': 56, 'bin_end': 58, 'count': 17019 }, { 'bin_start': 58, 'bin_end': 59, 'count': 13307 }, { 'bin_start': 59, 'bin_end': 61, 'count': 9364 }, { 'bin_start': 61, 'bin_end': 63, 'count': 3231 }, { 'bin_start': 63, 'bin_end': 65, 'count': 4391 }, { 'bin_start': 65, 'bin_end': 66, 'count': 2319 }, { 'bin_start': 66, 'bin_end': 68, 'count': 1253 }, { 'bin_start': 68, 'bin_end': 70, 'count': 367 }, { 'bin_start': 70, 'bin_end': 72, 'count': 495 }, { 'bin_start': 72, 'bin_end': 73, 'count': 154 }, { 'bin_start': 73, 'bin_end': 75, 'count': 40 }, { 'bin_start': 77, 'bin_end': 77, 'count': 1 }] },
    'ln_mn': { 'median': 50, 'breaks': { 'lower': [-17, 44, 47, 49, 51, 53, 56], 'higher': [44, 47, 49, 51, 53, 56, 74] }, 'histogram': [{ 'bin_start': -17, 'bin_end': -15, 'count': 1 }, { 'bin_start': -12, 'bin_end': -10, 'count': 2 }, { 'bin_start': -10, 'bin_end': -8, 'count': 1 }, { 'bin_start': -4, 'bin_end': -2, 'count': 1 }, { 'bin_start': -2, 'bin_end': -1, 'count': 4 }, { 'bin_start': -1, 'bin_end': 1, 'count': 1 }, { 'bin_start': 3, 'bin_end': 5, 'count': 1 }, { 'bin_start': 5, 'bin_end': 7, 'count': 2 }, { 'bin_start': 7, 'bin_end': 8, 'count': 2 }, { 'bin_start': 8, 'bin_end': 10, 'count': 5 }, { 'bin_start': 10, 'bin_end': 12, 'count': 3 }, { 'bin_start': 12, 'bin_end': 14, 'count': 3 }, { 'bin_start': 14, 'bin_end': 16, 'count': 5 }, { 'bin_start': 16, 'bin_end': 18, 'count': 5 }, { 'bin_start': 18, 'bin_end': 19, 'count': 6 }, { 'bin_start': 19, 'bin_end': 21, 'count': 8 }, { 'bin_start': 21, 'bin_end': 23, 'count': 26 }, { 'bin_start': 23, 'bin_end': 25, 'count': 12 }, { 'bin_start': 25, 'bin_end': 27, 'count': 17 }, { 'bin_start': 27, 'bin_end': 29, 'count': 19 }, { 'bin_start': 29, 'bin_end': 30, 'count': 27 }, { 'bin_start': 30, 'bin_end': 32, 'count': 49 }, { 'bin_start': 32, 'bin_end': 34, 'count': 28 }, { 'bin_start': 34, 'bin_end': 36, 'count': 128 }, { 'bin_start': 36, 'bin_end': 38, 'count': 305 }, { 'bin_start': 38, 'bin_end': 39, 'count': 1144 }, { 'bin_start': 39, 'bin_end': 41, 'count': 3868 }, { 'bin_start': 41, 'bin_end': 43, 'count': 7504 }, { 'bin_start': 43, 'bin_end': 45, 'count': 4683 }, { 'bin_start': 45, 'bin_end': 47, 'count': 13079 }, { 'bin_start': 47, 'bin_end': 49, 'count': 16248 }, { 'bin_start': 49, 'bin_end': 50, 'count': 18370 }, { 'bin_start': 50, 'bin_end': 52, 'count': 18298 }, { 'bin_start': 52, 'bin_end': 54, 'count': 7735 }, { 'bin_start': 54, 'bin_end': 56, 'count': 11689 }, { 'bin_start': 56, 'bin_end': 58, 'count': 7812 }, { 'bin_start': 58, 'bin_end': 59, 'count': 4781 }, { 'bin_start': 59, 'bin_end': 61, 'count': 2620 }, { 'bin_start': 61, 'bin_end': 63, 'count': 1521 }, { 'bin_start': 63, 'bin_end': 65, 'count': 497 }, { 'bin_start': 65, 'bin_end': 67, 'count': 616 }, { 'bin_start': 67, 'bin_end': 69, 'count': 256 }, { 'bin_start': 69, 'bin_end': 70, 'count': 66 }, { 'bin_start': 70, 'bin_end': 72, 'count': 5 }, { 'bin_start': 74, 'bin_end': 74, 'count': 1 }] },
    'lden_mn': { 'median': 58, 'breaks': { 'lower': [-8, 52, 55, 57, 59, 61, 64], 'higher': [52, 55, 57, 59, 61, 64, 81] }, 'histogram': [{ 'bin_start': -8, 'bin_end': -6, 'count': 1 }, { 'bin_start': -3, 'bin_end': -1, 'count': 3 }, { 'bin_start': 4, 'bin_end': 6, 'count': 3 }, { 'bin_start': 6, 'bin_end': 8, 'count': 2 }, { 'bin_start': 10, 'bin_end': 12, 'count': 1 }, { 'bin_start': 12, 'bin_end': 13, 'count': 1 }, { 'bin_start': 13, 'bin_end': 15, 'count': 4 }, { 'bin_start': 15, 'bin_end': 17, 'count': 1 }, { 'bin_start': 17, 'bin_end': 19, 'count': 4 }, { 'bin_start': 19, 'bin_end': 20, 'count': 4 }, { 'bin_start': 20, 'bin_end': 22, 'count': 6 }, { 'bin_start': 22, 'bin_end': 24, 'count': 3 }, { 'bin_start': 24, 'bin_end': 26, 'count': 3 }, { 'bin_start': 26, 'bin_end': 28, 'count': 7 }, { 'bin_start': 28, 'bin_end': 29, 'count': 9 }, { 'bin_start': 29, 'bin_end': 31, 'count': 27 }, { 'bin_start': 31, 'bin_end': 33, 'count': 12 }, { 'bin_start': 33, 'bin_end': 35, 'count': 15 }, { 'bin_start': 35, 'bin_end': 37, 'count': 21 }, { 'bin_start': 37, 'bin_end': 38, 'count': 27 }, { 'bin_start': 38, 'bin_end': 40, 'count': 52 }, { 'bin_start': 40, 'bin_end': 42, 'count': 37 }, { 'bin_start': 42, 'bin_end': 44, 'count': 133 }, { 'bin_start': 44, 'bin_end': 45, 'count': 408 }, { 'bin_start': 45, 'bin_end': 47, 'count': 1619 }, { 'bin_start': 47, 'bin_end': 49, 'count': 1926 }, { 'bin_start': 49, 'bin_end': 51, 'count': 5985 }, { 'bin_start': 51, 'bin_end': 53, 'count': 8640 }, { 'bin_start': 53, 'bin_end': 54, 'count': 12141 }, { 'bin_start': 54, 'bin_end': 56, 'count': 15179 }, { 'bin_start': 56, 'bin_end': 58, 'count': 8684 }, { 'bin_start': 58, 'bin_end': 60, 'count': 18375 }, { 'bin_start': 60, 'bin_end': 61, 'count': 16605 }, { 'bin_start': 61, 'bin_end': 63, 'count': 12297 }, { 'bin_start': 63, 'bin_end': 65, 'count': 4732 }, { 'bin_start': 65, 'bin_end': 67, 'count': 6567 }, { 'bin_start': 67, 'bin_end': 69, 'count': 3753 }, { 'bin_start': 69, 'bin_end': 70, 'count': 2032 }, { 'bin_start': 70, 'bin_end': 72, 'count': 1198 }, { 'bin_start': 72, 'bin_end': 74, 'count': 366 }, { 'bin_start': 74, 'bin_end': 76, 'count': 404 }, { 'bin_start': 76, 'bin_end': 77, 'count': 140 }, { 'bin_start': 77, 'bin_end': 79, 'count': 26 }, { 'bin_start': 81, 'bin_end': 81, 'count': 1 }] },
    'legal_sleep': { 'median': 25, 'breaks': { 'lower': [25, 25, 25, 25, 25, 25, 30], 'higher': [25, 25, 25, 25, 25, 30, 42] }, 'histogram': [{ 'bin_start': 25, 'bin_end': 25, 'count': 87511 }, { 'bin_start': 30, 'bin_end': 30, 'count': 32147 }, { 'bin_start': 32, 'bin_end': 32, 'count': 741 }, { 'bin_start': 33, 'bin_end': 33, 'count': 970 }, { 'bin_start': 37, 'bin_end': 37, 'count': 225 }, { 'bin_start': 42, 'bin_end': 42, 'count': 25 }] },
    'legal_live': { 'median': 25, 'breaks': { 'lower': [25, 25, 25, 25, 25, 25, 30], 'higher': [25, 25, 25, 25, 25, 30, 37] }, 'histogram': [{ 'bin_start': 25, 'bin_end': 25, 'count': 87511 }, { 'bin_start': 30, 'bin_end': 30, 'count': 32888 }, { 'bin_start': 32, 'bin_end': 32, 'count': 225 }, { 'bin_start': 33, 'bin_end': 33, 'count': 970 }, { 'bin_start': 37, 'bin_end': 37, 'count': 25 }] },
    'sleep_mx': { 'median': 26, 'breaks': { 'lower': [-40, 20, 23, 25, 27, 29, 32], 'higher': [20, 23, 25, 27, 29, 32, 49] }, 'histogram': [{ 'bin_start': -40, 'bin_end': -38, 'count': 1 }, { 'bin_start': -33, 'bin_end': -31, 'count': 1 }, { 'bin_start': -28, 'bin_end': -26, 'count': 3 }, { 'bin_start': -26, 'bin_end': -24, 'count': 2 }, { 'bin_start': -22, 'bin_end': -20, 'count': 1 }, { 'bin_start': -20, 'bin_end': -19, 'count': 1 }, { 'bin_start': -19, 'bin_end': -17, 'count': 1 }, { 'bin_start': -17, 'bin_end': -15, 'count': 2 }, { 'bin_start': -15, 'bin_end': -13, 'count': 1 }, { 'bin_start': -13, 'bin_end': -12, 'count': 1 }, { 'bin_start': -12, 'bin_end': -10, 'count': 6 }, { 'bin_start': -10, 'bin_end': -8, 'count': 2 }, { 'bin_start': -8, 'bin_end': -6, 'count': 6 }, { 'bin_start': -6, 'bin_end': -4, 'count': 7 }, { 'bin_start': -4, 'bin_end': -3, 'count': 15 }, { 'bin_start': -3, 'bin_end': -1, 'count': 19 }, { 'bin_start': -1, 'bin_end': 1, 'count': 9 }, { 'bin_start': 1, 'bin_end': 3, 'count': 23 }, { 'bin_start': 3, 'bin_end': 5, 'count': 14 }, { 'bin_start': 5, 'bin_end': 6, 'count': 31 }, { 'bin_start': 6, 'bin_end': 8, 'count': 62 }, { 'bin_start': 8, 'bin_end': 10, 'count': 86 }, { 'bin_start': 10, 'bin_end': 12, 'count': 333 }, { 'bin_start': 12, 'bin_end': 13, 'count': 813 }, { 'bin_start': 13, 'bin_end': 15, 'count': 2246 }, { 'bin_start': 15, 'bin_end': 17, 'count': 2072 }, { 'bin_start': 17, 'bin_end': 19, 'count': 6206 }, { 'bin_start': 19, 'bin_end': 21, 'count': 9276 }, { 'bin_start': 21, 'bin_end': 22, 'count': 12508 }, { 'bin_start': 22, 'bin_end': 24, 'count': 15257 }, { 'bin_start': 24, 'bin_end': 26, 'count': 8645 }, { 'bin_start': 26, 'bin_end': 28, 'count': 18015 }, { 'bin_start': 28, 'bin_end': 29, 'count': 15528 }, { 'bin_start': 29, 'bin_end': 31, 'count': 11752 }, { 'bin_start': 31, 'bin_end': 33, 'count': 4317 }, { 'bin_start': 33, 'bin_end': 35, 'count': 6080 }, { 'bin_start': 35, 'bin_end': 37, 'count': 3562 }, { 'bin_start': 37, 'bin_end': 38, 'count': 2154 }, { 'bin_start': 38, 'bin_end': 40, 'count': 1319 }, { 'bin_start': 40, 'bin_end': 42, 'count': 415 }, { 'bin_start': 42, 'bin_end': 44, 'count': 481 }, { 'bin_start': 44, 'bin_end': 45, 'count': 169 }, { 'bin_start': 45, 'bin_end': 47, 'count': 11 }, { 'bin_start': 49, 'bin_end': 49, 'count': 1 }] },
    'live_mx': { 'median': 31, 'breaks': { 'lower': [-35, 25, 28, 30, 32, 34, 37], 'higher': [25, 28, 30, 32, 34, 37, 53] }, 'histogram': [{ 'bin_start': -35, 'bin_end': -33, 'count': 1 }, { 'bin_start': -26, 'bin_end': -24, 'count': 1 }, { 'bin_start': -23, 'bin_end': -21, 'count': 3 }, { 'bin_start': -19, 'bin_end': -17, 'count': 2 }, { 'bin_start': -17, 'bin_end': -16, 'count': 1 }, { 'bin_start': -16, 'bin_end': -14, 'count': 1 }, { 'bin_start': -12, 'bin_end': -10, 'count': 3 }, { 'bin_start': -10, 'bin_end': -9, 'count': 2 }, { 'bin_start': -9, 'bin_end': -7, 'count': 2 }, { 'bin_start': -7, 'bin_end': -5, 'count': 4 }, { 'bin_start': -5, 'bin_end': -3, 'count': 2 }, { 'bin_start': -3, 'bin_end': -2, 'count': 7 }, { 'bin_start': -2, 'bin_end': 0, 'count': 12 }, { 'bin_start': 0, 'bin_end': 2, 'count': 7 }, { 'bin_start': 2, 'bin_end': 4, 'count': 16 }, { 'bin_start': 4, 'bin_end': 5, 'count': 15 }, { 'bin_start': 5, 'bin_end': 7, 'count': 21 }, { 'bin_start': 7, 'bin_end': 9, 'count': 9 }, { 'bin_start': 9, 'bin_end': 11, 'count': 26 }, { 'bin_start': 11, 'bin_end': 13, 'count': 83 }, { 'bin_start': 13, 'bin_end': 14, 'count': 175 }, { 'bin_start': 14, 'bin_end': 16, 'count': 359 }, { 'bin_start': 16, 'bin_end': 18, 'count': 367 }, { 'bin_start': 18, 'bin_end': 20, 'count': 1387 }, { 'bin_start': 20, 'bin_end': 21, 'count': 3143 }, { 'bin_start': 21, 'bin_end': 23, 'count': 5705 }, { 'bin_start': 23, 'bin_end': 25, 'count': 4027 }, { 'bin_start': 25, 'bin_end': 27, 'count': 10457 }, { 'bin_start': 27, 'bin_end': 28, 'count': 13141 }, { 'bin_start': 28, 'bin_end': 30, 'count': 15690 }, { 'bin_start': 30, 'bin_end': 32, 'count': 8642 }, { 'bin_start': 32, 'bin_end': 34, 'count': 17085 }, { 'bin_start': 34, 'bin_end': 35, 'count': 14647 }, { 'bin_start': 35, 'bin_end': 37, 'count': 10737 }, { 'bin_start': 37, 'bin_end': 39, 'count': 3881 }, { 'bin_start': 39, 'bin_end': 41, 'count': 5316 }, { 'bin_start': 41, 'bin_end': 42, 'count': 3035 }, { 'bin_start': 42, 'bin_end': 44, 'count': 1776 }, { 'bin_start': 44, 'bin_end': 46, 'count': 600 }, { 'bin_start': 46, 'bin_end': 48, 'count': 701 }, { 'bin_start': 48, 'bin_end': 49, 'count': 301 }, { 'bin_start': 49, 'bin_end': 51, 'count': 62 }, { 'bin_start': 51, 'bin_end': 53, 'count': 1 }, { 'bin_start': 53, 'bin_end': 53, 'count': 1 }] },
    'sleep_mn': { 'median': 24, 'breaks': { 'lower': [-42, 17, 20, 22, 25, 27, 30], 'higher': [17, 20, 22, 25, 27, 30, 49] }, 'histogram': [{ 'bin_start': -42, 'bin_end': -40, 'count': 1 }, { 'bin_start': -40, 'bin_end': -38, 'count': 3 }, { 'bin_start': -33, 'bin_end': -31, 'count': 1 }, { 'bin_start': -29, 'bin_end': -27, 'count': 1 }, { 'bin_start': -27, 'bin_end': -26, 'count': 4 }, { 'bin_start': -26, 'bin_end': -24, 'count': 4 }, { 'bin_start': -24, 'bin_end': -22, 'count': 2 }, { 'bin_start': -22, 'bin_end': -20, 'count': 1 }, { 'bin_start': -18, 'bin_end': -17, 'count': 2 }, { 'bin_start': -17, 'bin_end': -15, 'count': 4 }, { 'bin_start': -15, 'bin_end': -13, 'count': 6 }, { 'bin_start': -13, 'bin_end': -11, 'count': 2 }, { 'bin_start': -11, 'bin_end': -9, 'count': 7 }, { 'bin_start': -9, 'bin_end': -7, 'count': 6 }, { 'bin_start': -7, 'bin_end': -6, 'count': 29 }, { 'bin_start': -6, 'bin_end': -4, 'count': 9 }, { 'bin_start': -4, 'bin_end': -2, 'count': 18 }, { 'bin_start': -2, 'bin_end': 0, 'count': 12 }, { 'bin_start': 0, 'bin_end': 2, 'count': 30 }, { 'bin_start': 2, 'bin_end': 4, 'count': 38 }, { 'bin_start': 4, 'bin_end': 5, 'count': 89 }, { 'bin_start': 5, 'bin_end': 7, 'count': 160 }, { 'bin_start': 7, 'bin_end': 9, 'count': 150 }, { 'bin_start': 9, 'bin_end': 11, 'count': 699 }, { 'bin_start': 11, 'bin_end': 13, 'count': 1490 }, { 'bin_start': 13, 'bin_end': 14, 'count': 3316 }, { 'bin_start': 14, 'bin_end': 16, 'count': 7025 }, { 'bin_start': 16, 'bin_end': 18, 'count': 10898 }, { 'bin_start': 18, 'bin_end': 20, 'count': 6178 }, { 'bin_start': 20, 'bin_end': 22, 'count': 14579 }, { 'bin_start': 22, 'bin_end': 24, 'count': 15475 }, { 'bin_start': 24, 'bin_end': 25, 'count': 15782 }, { 'bin_start': 25, 'bin_end': 27, 'count': 15158 }, { 'bin_start': 27, 'bin_end': 29, 'count': 6328 }, { 'bin_start': 29, 'bin_end': 31, 'count': 9418 }, { 'bin_start': 31, 'bin_end': 33, 'count': 6303 }, { 'bin_start': 33, 'bin_end': 34, 'count': 3801 }, { 'bin_start': 34, 'bin_end': 36, 'count': 2089 }, { 'bin_start': 36, 'bin_end': 38, 'count': 1174 }, { 'bin_start': 38, 'bin_end': 40, 'count': 392 }, { 'bin_start': 40, 'bin_end': 42, 'count': 480 }, { 'bin_start': 42, 'bin_end': 44, 'count': 222 }, { 'bin_start': 44, 'bin_end': 45, 'count': 63 }, { 'bin_start': 45, 'bin_end': 47, 'count': 4 }, { 'bin_start': 49, 'bin_end': 49, 'count': 1 }] },
    'live_mn': { 'median': 29, 'breaks': { 'lower': [-35, 22, 25, 27, 30, 32, 35], 'higher': [22, 25, 27, 30, 32, 35, 53] }, 'histogram': [{ 'bin_start': -35, 'bin_end': -33, 'count': 3 }, { 'bin_start': -33, 'bin_end': -31, 'count': 1 }, { 'bin_start': -26, 'bin_end': -24, 'count': 2 }, { 'bin_start': -23, 'bin_end': -21, 'count': 5 }, { 'bin_start': -19, 'bin_end': -17, 'count': 4 }, { 'bin_start': -17, 'bin_end': -16, 'count': 2 }, { 'bin_start': -16, 'bin_end': -14, 'count': 1 }, { 'bin_start': -12, 'bin_end': -10, 'count': 4 }, { 'bin_start': -10, 'bin_end': -9, 'count': 3 }, { 'bin_start': -9, 'bin_end': -7, 'count': 7 }, { 'bin_start': -7, 'bin_end': -5, 'count': 7 }, { 'bin_start': -5, 'bin_end': -3, 'count': 14 }, { 'bin_start': -3, 'bin_end': -2, 'count': 18 }, { 'bin_start': -2, 'bin_end': 0, 'count': 9 }, { 'bin_start': 0, 'bin_end': 2, 'count': 10 }, { 'bin_start': 2, 'bin_end': 4, 'count': 14 }, { 'bin_start': 4, 'bin_end': 5, 'count': 26 }, { 'bin_start': 5, 'bin_end': 7, 'count': 40 }, { 'bin_start': 7, 'bin_end': 9, 'count': 33 }, { 'bin_start': 9, 'bin_end': 11, 'count': 110 }, { 'bin_start': 11, 'bin_end': 13, 'count': 281 }, { 'bin_start': 13, 'bin_end': 14, 'count': 634 }, { 'bin_start': 14, 'bin_end': 16, 'count': 1372 }, { 'bin_start': 16, 'bin_end': 18, 'count': 1241 }, { 'bin_start': 18, 'bin_end': 20, 'count': 4829 }, { 'bin_start': 20, 'bin_end': 21, 'count': 7613 }, { 'bin_start': 21, 'bin_end': 23, 'count': 10594 }, { 'bin_start': 23, 'bin_end': 25, 'count': 5746 }, { 'bin_start': 25, 'bin_end': 27, 'count': 12886 }, { 'bin_start': 27, 'bin_end': 28, 'count': 14106 }, { 'bin_start': 28, 'bin_end': 30, 'count': 14703 }, { 'bin_start': 30, 'bin_end': 32, 'count': 7323 }, { 'bin_start': 32, 'bin_end': 34, 'count': 13232 }, { 'bin_start': 34, 'bin_end': 35, 'count': 10355 }, { 'bin_start': 35, 'bin_end': 37, 'count': 7172 }, { 'bin_start': 37, 'bin_end': 39, 'count': 2358 }, { 'bin_start': 39, 'bin_end': 41, 'count': 3225 }, { 'bin_start': 41, 'bin_end': 42, 'count': 1720 }, { 'bin_start': 42, 'bin_end': 44, 'count': 950 }, { 'bin_start': 44, 'bin_end': 46, 'count': 297 }, { 'bin_start': 46, 'bin_end': 48, 'count': 354 }, { 'bin_start': 48, 'bin_end': 49, 'count': 123 }, { 'bin_start': 49, 'bin_end': 51, 'count': 26 }, { 'bin_start': 53, 'bin_end': 53, 'count': 1 }] },
  };

  madb.data.risks ={
    // http://gesdoc.isciii.es/gesdoccontroller?action=download&id=18/10/2016-72b28c0577
    emergency:[
      {
        desc:'Total causas naturales (exclueyendo accidentes y partos)',
        param:'ld',
        factor:1,
        mult: 5.1
      },{
        desc:'Por problemas cardiovasculares',
        param:'ld',
        factor:1,
        mult: 4.2
      },{
        desc:'Por problemas respiratorios',
        param:'ld',
        factor:1,
        mult: 3.7
      }
    ],
    emergency_kids:[
      {
        desc:'Total causas naturales (exclueyendo accidentes y partos)',
        param:'lden',
        factor:1,
        mult: 2.4
      },{
        desc:'Por problemas respiratorios',
        param:'lden',
        factor:1,
        mult: 4.7
      },{
        desc:'Neumonía',
        param:'lden',
        factor:1,
        mult: 7.7
      }
    ],
    calls_112:[
      {
        desc:'Por problemas orgánicos',
        param:'ln',
        factor:1,
        mult: 9.7
      },{
        desc:'Por problemas cardiovasculares',
        param:'ln',
        factor:1,
        mult: 7.2
      },{
        desc:'Por problemas respiratorios',
        param:'ln',
        factor:1,
        mult: 12.4
      }
    ],
    mortality_heart:[
      {
        desc:'Total causas cardiovaculares',
        param:'ln',
        factor:1,
        mult: 5
      },{
        desc:'Cardiopatía isquémica',
        param:'ln',
        factor:1,
        mult: 11
      },{
        desc:'Infarto agudo de miocardio',
        param:'ln',
        factor:1,
        mult: 11
      }
    ],
    mortality_heart_65plus:[
      {
        desc:'Total causas cardiovaculares',
        param:'ln',
        factor:1,
        mult: 3.3
      },{
        desc:'Cardiopatía isquémica',
        param:'ln',
        factor:1,
        mult: 2.9
      },{
        desc:'Infarto agudo de miocardio',
        param:'ln',
        factor:1,
        mult: 3.5
      },{
        desc:'Accidente cerebrovascular',
        param:'ln',
        factor:1,
        mult: 2.4
      }
    ],
    mortality_respiratory_65plus:[
      {
        desc:'Total causas respiratorias',
        param:'ld',
        factor:1,
        mult: 6.5
      },{
        desc:'Total causas respiratorias, al día siguiente',
        param:'ln',
        factor:1,
        mult: 2.2
      },{
        desc:'Neumonía, al día siguiente',
        param:'ln',
        factor:1,
        mult: 3
      },{
        desc:'EPOC, al día siguiente',
        param:'ln',
        factor:1,
        mult: 4
      }
    ],
    mortality_diabetes_65plus:[
      {
        desc:'Al día siguiente',
        param:'ln',
        factor:1,
        mult: 11
      }
    ],
    adverse_births:[
      {
        desc:'Total partos prematuros',
        param:'ld',
        factor:1,
        mult: 3.2 
      },{
        desc:'Partos prematuros en el 2º trimestre',
        param:'ld',
        factor:1,
        mult: 3 
      },{
        desc:'Partos prematuros en el 3º trimestre',
        param:'ln',
        factor:1,
        mult: 1.9
      },{
        desc:'Bajo peso del recién nacido',
        param:'ld',
        factor:1,
        mult: 6.4
      },{
        desc:'Mortalidad del recién nacido en las primeras 24h',
        param:'ld',
        factor:1,
        mult: 6
      }
    ],
    hospital_admissions:[
      {
        desc:'Ingresos por demencia',
        param:'ld',
        factor:1,
        mult: 13
      },
      // https://www.isciii.es/Noticias/Noticias/Documents/0170914Esclerosisyruido.pdf#search=ruido%20madrid
      {
        desc:'Ingresos urgentes por esclerosis múltiple',
        param:'ld',
        factor:1,
        mult: 1.21,
        threshold: 67,
        mult2: 1.62
      },{
        desc:'Ingresos urgentes por Parkinson',
        param:'ld',
        factor:1,
        mult: 11.4
      },{
        desc:'Llamadas al 112 por Parkinson, al día siguiente',
        param:'ln',
        factor:1,
        mult: 31.4
      }
    ],
    // https://doi.org/10.1016/j.scitotenv.2019.136315
    psychological_effects:[
      {
        desc:'Suicidios, al día siguiente',
        param:'ld',
        factor:1,
        mult: 14.5
      },{
        desc:'Ingresos de urgencia por ansiedad',
        param:'ld',
        factor:1,
        mult: 16.7
      },{
        desc:'Ingresos de urgencia por depresión',
        param:'ld',
        factor:1,
        mult: 9.9
      },
    ],
    // http://www.euro.who.int/en/health-topics/environment-and-health/noise/publications/2009/night-noise-guidelines-for-europe
    sleep_effects:[
      {
        desc:'Aumento de la actividad motora',
        param:'sleep',
        func: k =>  0.000192 * k
      },{
        desc:'Perturbaciones del sueño',
        param:'ln',
        func: k =>  13.8 - 0.85 * k + 0.01670 * k * k
      },
    ]
  };

  madb.data.histogram = (container, o) => {
    const
      spec = {
        '$schema': 'https://vega.github.io/schema/vega-lite/v4.json',
        'data': {
          'values': madb.data.raw
        },
        'transform': [{
          'calculate': `1/${madb.data.raw.length}`,
          'as': 'PercentOfTotal'
        }],
        'mark': 'bar',
        'width': 'container',
        'height': 'container',
        'encoding': {
          'x': {
            'bin': true,
            'field': o.x,
            'type': 'quantitative',
            'axis': { 'title': o.xlabel, 'format': 'd' }
          },
          'y': {
            'aggregate': 'sum',
            'field': 'PercentOfTotal',
            'type': 'quantitative',
            'axis': { 'title': null, 'format': '0%' }
          },
          'color': {
            'field': 'count',
            'scale': { 'scheme': 'teals' },
            'legend': null
          }
        },
        'config': {
          'view': {
            'stroke': 'transparent'
          }
        }
      };
    vegaEmbed(container, spec, { actions: false });
  };

  madb.data.heatmap = (container, o) => {
    const
      mxbins = 25,
      d = madb.data.raw.map(k => { return { day: k[o.x], night: k[o.y] };}),
      spec = {
        '$schema': 'https://vega.github.io/schema/vega-lite/v4.json',
        'title': o.title,
        'data': {
          'values': d
        },
        'transform': [
          {
            'calculate': `1/${madb.data.raw.length}`,
            'as': 'PercentOfTotal'
          },
          {
            'calculate': '45',
            'as': 'nmx'
          },
          {
            'calculate': '53',
            'as': 'dmx'
          }
        ],
        'width': 'container',
        'height': 'container',
        'layer': [
          {
            'mark': 'rect',
            'encoding': {
              'x': {
                'bin': { 'maxbins': mxbins },
                'field': 'day',
                'type': 'quantitative',
                'axis': { 'title': o.xlabel }
              },
              'y': {
                'bin': { 'maxbins': mxbins },
                'field': 'night',
                'type': 'quantitative',
                'axis': { 'title': o.ylabel }
              },
              'color': {
                //'aggregate': 'count',
                'aggregate': 'sum',
                'field': 'PercentOfTotal',
                'type': 'quantitative',
                'scale': { 'scheme': 'teals' },
                'legend': {
                  'title': null,
                  'format': '0%'
                }
              }
            },
            'config': {
              'view': {
                'stroke': 'transparent'
              }
            }
          },
          {
            'mark': 'rule',
            'encoding': {
              'x': {
                'aggregate': 'max',
                'field': 'dmx',
                'type': 'quantitative'
              },
              'color': { 'value': '#ff1d8e' },
              'size': { 'value': 0.5 }
            }
          },
          {
            'mark': 'rule',
            'encoding': {
              'y': {
                'aggregate': 'max',
                'field': 'nmx',
                'type': 'quantitative'
              },
              'color': { 'value': '#ff1d8e' },
              'size': { 'value': 0.5 }
            }
          }
        ]
      };
    vegaEmbed(container, spec, { actions: false });
  };

})();