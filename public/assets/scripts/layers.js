/*jshint esversion: 6 */
(function () {

    'use strict';

    window.madb = window.madb || {};

    madb.style = {};

    const
        change = () =>{
            let
                inc = -3,
                exposed = madb.style.exposed,
                inner = madb.style.inner,
                loc = (inner)? '<i class="icofont-external"></i> interior' : '<i class="icofont-external-link"></i> exterior',
                exp = (exposed)? '<span class=" as-hot">más expuesta</span>':'<span class=" as-info">menos expuesta</span>',
                label = `Ruido <span class="as-color--type-01 as-font--medium">${loc}</span> en la fachada <span class="as-font--medium">${exp}</span>`,
                pre = (inner)? ['live', 'sleep'] : ['ld', 'ln'],
                post = (exposed)? '_mx':'_mn',
                breaks_d = madb.data.global[pre[0]+post].breaks.lower,
                breaks_n = madb.data.global[pre[1]+post].breaks.lower,
                dstyle = [
                    'interpolate-lab',
                    ['linear'],
                    ['get', pre[0]+post],
                    breaks_d[1], '#007080',
                    breaks_d[6], '#ff1d8e'
                ],
                nstyle = [
                    'interpolate-lab',
                    ['linear'],
                    ['get', pre[1]+post],
                    breaks_n[1], '#007080',
                    breaks_n[6], '#ff1d8e'
                ];
            madb.day.setPaintProperty('buildings', 'fill-color', dstyle);
            madb.night.setPaintProperty('buildings', 'fill-color', nstyle);
            madb.day.setPaintProperty('3d-buildings', 'fill-extrusion-color', dstyle);
            madb.night.setPaintProperty('3d-buildings', 'fill-extrusion-color', nstyle);
            document.querySelector('.layername').innerHTML = label;
            document.querySelector('.scaleday .lower').innerText = `${breaks_d[1] + inc}dB`;
            document.querySelector('.scaleday .higher').innerText = `${breaks_d[6] + inc}dB`;
            document.querySelector('.scalenight .lower').innerText = `${breaks_n[1] + inc}dB`;
            document.querySelector('.scalenight .higher').innerText = `${breaks_n[6] + inc}dB`;
        }, 
        app_path = location.href.replace(/[^/]*$/, ''),
        labels = ['dark_only_labels', 'light_only_labels', 'rastertiles/voyager_only_labels'],
        colors = ["#007080", "#546e82", "#7f6984", "#a16287", "#c25689", "#e0448b", "#ff1d8e"],
        day = {
            'version': 8,
            'name': 'MadB_Day',
            'center': [-3.691944, 40.418889],
            'zoom': 11,
            'sources': {
                'parks': {
                    'type': 'vector',
                    'tiles': [`${app_path}tiles/parks/{z}/{x}/{y}.pbf`],
                    'maxzoom': 15
                },
                'buildings': {
                    'type': 'vector',
                    'tiles': [`${app_path}tiles/buildings/{z}/{x}/{y}.pbf`],
                    'maxzoom': 15
                },
                'labels': {
                    'type': 'vector',
                    'tiles': [`${app_path}tiles/calles/{z}/{x}/{y}.pbf`],
                    'maxzoom': 13
                }
            },
            "glyphs": `${app_path}tiles/font/{fontstack}/{range}.pbf`,
            'layers': [
                {
                    'id': 'background',
                    'type': 'background',
                    'paint': {
                        'background-color': '#8a99ac'//'#b7c0cc'
                    }
                },
                {
                    'id': 'parks',
                    'type': 'fill',
                    'source': 'parks',
                    'source-layer': 'parks',
                    'paint': {
                        'fill-color': '#084408',//'#061a06',
                        "fill-opacity": 0.3,
                        'fill-outline-color': '#87c287'
                    }
                },
                {
                    'id': 'buildings',
                    'type': 'fill',
                    'source': 'buildings',
                    'source-layer': 'building_noise_height',
                    'maxzoom': 15,
                    'paint': {
                        'fill-color': [
                            'interpolate-lab',
                            ['linear'],
                            ['get', 'ld_mx'],
                            52, '#007080',
                            63, '#ff1d8e'
                        ]
                    }
                },
                {
                    'id': 'labels',
                    'type': 'symbol',
                    'source': 'labels',
                    'source-layer':'calles',
                    'minzoom': 13,
                    'maxzoom': 22,
                    "layout": {
                      "symbol-placement": "line",
                      "text-font":  ["Open Sans Semibold"],
                      "text-size": {
                        "stops": [
                          [
                            13,
                            9
                          ],
                          [
                            15,
                            10
                          ],
                          [
                            16,
                            11
                          ],
                          [
                            18,
                            12
                          ]
                        ]
                      },
                      "text-field": ['get', 'nombre'],
                      "symbol-avoid-edges": false,
                      "symbol-spacing": {
                        "stops": [
                            [
                              6,
                              200
                            ],
                            [
                              16,
                              250
                            ]
                          ]
                      },
                      "text-pitch-alignment": "auto",
                      "text-rotation-alignment": "auto",
                      "text-justify": "center",
                      "text-letter-spacing": {
                        "stops": [
                          [
                            13,
                            0
                          ],
                          [
                            16,
                            0.2
                          ]
                        ]
                      }
                    },
                    "paint": {
                      "text-color": "#333",
                      "text-halo-color": "#fff",
                      "text-halo-width": 1
                    }
                },
                {
                    'id': '3d-buildings',
                    'source': 'buildings',
                    'source-layer': 'building_noise_height',
                    'type': 'fill-extrusion',
                    'minzoom': 15,
                    'paint': {
                        'fill-extrusion-color': [
                            'interpolate-lab',
                            ['linear'],
                            ['get', 'ld_mx'],
                            52, '#007080',
                            63, '#ff1d8e'
                        ],
                        'fill-extrusion-height': [
                            'interpolate',
                            ['linear'],
                            ['zoom'],
                            15,
                            0,
                            15.05,
                            ['*', 4,['get', 'floors']]
                        ],
                        'fill-extrusion-opacity': 1
                    }
                }
            ]
        },
        night = {
            'version': 8,
            'name': 'MadB_Night',
            'center': [-3.691944, 40.418889],
            'zoom': 11,
            'sources': {
                'parks': {
                    'type': 'vector',
                    'tiles': [`${app_path}tiles/parks/{z}/{x}/{y}.pbf`],
                    'maxzoom': 15
                },
                'buildings': {
                    'type': 'vector',
                    'tiles': [`${app_path}tiles/buildings/{z}/{x}/{y}.pbf`],
                    'maxzoom': 15
                },
                'labels': {
                    'type': 'vector',
                    'tiles': [`${app_path}tiles/calles/{z}/{x}/{y}.pbf`],
                    'maxzoom': 13
                }
            },
            "glyphs": `${app_path}tiles/font/{fontstack}/{range}.pbf`,
            'layers': [
                {
                    'id': 'background',
                    'type': 'background',
                    'paint': {
                        'background-color': '#36404d' //'#2D343E'
                    }
                },
                {
                    'id': 'parks',
                    'type': 'fill',
                    'source': 'parks',
                    'source-layer': 'parks',
                    'paint': {
                        'fill-color': '#084408',
                        "fill-opacity": 0.3,
                        'fill-outline-color': '#155115'
                    }
                },
                {
                    'id': 'buildings',
                    'type': 'fill',
                    'source': 'buildings',
                    'source-layer': 'building_noise_height',
                    'maxzoom': 15,
                    'paint': {
                        'fill-color': [
                            'interpolate-lab',
                            ['linear'],
                            ['get', 'ln_mx'],
                            47, '#007080',
                            58, '#ff1d8e'
                        ]
                    }
                },
                {
                    'id': 'labels',
                    'type': 'symbol',
                    'source': 'labels',
                    'source-layer':'calles',
                    'minzoom': 13,
                    'maxzoom': 22,
                    "layout": {
                      "symbol-placement": "line",
                      "text-font":  ["Open Sans Semibold"],
                      "text-size": {
                        "stops": [
                          [
                            13,
                            9
                          ],
                          [
                            15,
                            10
                          ],
                          [
                            16,
                            11
                          ],
                          [
                            18,
                            12
                          ]
                        ]
                      },
                      "text-field": ['get', 'nombre'],
                      "symbol-avoid-edges": false,
                      "symbol-spacing": {
                        "stops": [
                            [
                              6,
                              200
                            ],
                            [
                              16,
                              250
                            ]
                          ]
                      },
                      "text-pitch-alignment": "auto",
                      "text-rotation-alignment": "auto",
                      "text-justify": "center",
                      "text-letter-spacing": {
                        "stops": [
                          [
                            13,
                            0
                          ],
                          [
                            16,
                            0.2
                          ]
                        ]
                      }
                    },
                    "paint": {
                        "text-color": "#aaa",
                        "text-halo-color": "#181818",
                        "text-halo-width": 1
                    }
                },
                {
                    'id': '3d-buildings',
                    'source': 'buildings',
                    'source-layer': 'building_noise_height',
                    'type': 'fill-extrusion',
                    'minzoom': 15,
                    'paint': {
                        'fill-extrusion-color': [
                            'interpolate-lab',
                            ['linear'],
                            ['get', 'ln_mx'],
                            47, '#007080',
                            58, '#ff1d8e'
                        ],
                        'fill-extrusion-height': [
                            'interpolate',
                            ['linear'],
                            ['zoom'],
                            15,
                            0,
                            15.05,
                            ['*', 4,['get', 'floors']]
                        ],
                        'fill-extrusion-opacity': 1
                    }
                }
            ]
        };

window.madb.style = { day: day, night: night, exposed:true, inner:false, change:change};

}) ();