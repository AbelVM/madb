# MAdB
[![License: NPOSL-3.0](https://img.shields.io/badge/License_for_Code-NPOSL--3.0-008080)](https://opensource.org/licenses/NPOSL-3.0)
[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License_for_Data-CC%20BY--NC--SA%204.0-008080)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
[![Netlify Status](https://api.netlify.com/api/v1/badges/44db6039-e3bc-4219-9c6e-97af32004012/deploy-status)](https://app.netlify.com/sites/thirsty-bell-6c2e8c/deploys)

[Abel Vázquez Montoro](https://www.linkedin.com/in/abelvazquez/) 2020

[![img](public/assets/img/madb.svg)](https://madb.netlify.com/)

## Qué es esto?

Es el código fuente y los datos usados en [![MAdB](https://img.shields.io/badge/MA-dB-ff1d8e?labelColor=008080)](https://madb.netlify.com/), una herramienta para dar visibilidad a los efectos sobre la salud del ruido del tráfico en la ciudad de Madrid.

Es una aplicación web **autocontenida**: en este repositorio tienes todo el código y los datos para hacerla funcionar. No tiene dependencias externas de ninguna librería o servicio, no requiere de compilación y no necesita un despliegue ni herramientas de servidor.

Para ejecutarla en local, como siempre, por el dichoso [CORS](https://developer.mozilla.org/es/docs/Web/HTTP/Access_control_CORS), necesitas levantar un servidor web local (como `SimpleHTTPServer` de python, `http-server` de nodejs, `Live Server` dentro de VS Code, o el que más te guste).

## Licencias

### Los datos

Los **datos** de [![MAdB](https://img.shields.io/badge/MA-dB-ff1d8e?labelColor=008080)](https://madb.netlify.com/) están bajo licencia [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0).

### El código

El **código** de [![MAdB](https://img.shields.io/badge/MA-dB-ff1d8e?labelColor=008080)](https://madb.netlify.com/) está bajo licencia [Non-Profit Open Software License version 3.0](https://opensource.org/licenses/NPOSL-3.0).